<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 mb-0 font-weight-bold">
            {{ $header }}
        </h2>
    </x-slot>

    @if (isset($toolbar_left) OR isset($toolbar_right))
        <x-ui::toolbar class="row justify-content-between mb-5">

            @if (isset($toolbar_left))
                <x-ui::col class="col">
                    {{ $toolbar_left }}
                </x-ui::col>
            @endif

            @if (isset($toolbar_right))
                <x-ui::col class="col text-right">
                    {{ $toolbar_right }}
                </x-ui::col>
            @endif

        </x-ui::toolbar>
    @endif

    <x-ui::form-section
        method="{{ $method ?? 'post' }}"
    >
        <x-slot name="title">{{ $title }}</x-slot>

        @if (isset($description))
        <x-slot name="description">{{ $description }}</x-slot>
        @endif

        <x-slot name="action">
            {{ $action }}
        </x-slot>

        {{ $slot }}

        @if (! isset($actions))
            <x-slot name="actions">
                <x-ui::btn type="submit">
                    Simpan
                </x-ui::btn>
            </x-slot>
        @else
            {{ $actions }}
        @endif
    </x-ui::form-section>

</x-app-layout>
