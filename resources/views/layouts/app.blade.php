<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">

        <title>{{ config('app.name', 'SIZA.my') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}">
        {!! userCss() !!}
        @stack('styles')

        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        <script src="{{ mix('js/dashboard.js') }}" defer></script>
    </head>
    <body class="c-app font-sans antialiased">
        <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
            <div class="c-sidebar-brand">
                <a href="{{ config('siza-foundation.url') }}/dashboard">
{{--                    <x-jet-application-mark class="c-sidebar-brand-minimized" width="36" />--}}
{{--                    <x-jet-application-mark class="c-sidebar-brand-full" width="36" />--}}
                    <x-siza::ppz-logo-white class="c-sidebar-brand-full py-2" width="70" />
                </a>
            </div>
            <div class="c-sidebar-brand bg-gradient-success">
                <a href="/" class="text-white font">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <ul class="c-sidebar-nav">
                {{ $sidebar ?? '' }}
            </ul>

            <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
        </div>
        <div class="c-wrapper">
            <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
{{--                <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">--}}
{{--                    <span class="c-header-toggler-icon"></span>--}}
{{--                </button>--}}

{{--                <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">--}}
{{--                    <span class="c-header-toggler-icon"></span>--}}
{{--                </button>--}}

                <ul class="c-header-nav d-md-down-none">
                    <li class="c-header-nav-item px-3">
                        <x-jet-nav-link href="{{ config('siza-foundation.url') }}/dashboard">
                            <i class="fa fa-chevron-circle-left fa-fw mr-2"></i>
                            {{ __('Kembali ke SIZA.my') }}
                        </x-jet-nav-link>
                    </li>

                    <x-jet-dropdown id="appModuleDropdown" class="mr-4">
                        <x-slot name="trigger">
                            Modul SIZA.my

{{--                            <svg class="ml-2" width="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">--}}
{{--                                <path fill-rule="evenodd" d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />--}}
{{--                            </svg>--}}
                            <x-ui::icon.chevron-down />
                        </x-slot>

                        <x-slot name="content">
                            @foreach (appModules() as $module)
                                @php
                                $id = md5(microtime());
                                @endphp
                                <x-jet-dropdown-link href="{{ $module['url'] }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('module-{{ $id }}').submit();">
                                    {{ $module['name'] }}
                                </x-jet-dropdown-link>

                                <form id="module-{{ $id }}" action="{{ $module['url'].'/callback/login' }}" method="post" style="display: none;">
                                    <input type="hidden" name="id" value="{{ auth()->user()->emp_id }}">
                                    <input type="hidden" name="key" value="{{ auth()->user()->auth_key }}">
                                    <input type="hidden" name="impersonate_id" value="{{ session('impersonate_id') }}">
                                    <input type="hidden" name="kaunter" value="{{ session('kaunter') }}">
                                </form>
                            @endforeach
                        </x-slot>
                    </x-jet-dropdown>
                </ul>

                @livewire('navigation-menu')

                <div class="c-subheader px-3 py-3">
                    <div class="container">
                        {{ $header }}
                    </div>
                </div>
            </header>

            <div class="c-body">
                <main class="c-main">

                    <div class="{{ $container ?? 'container' }}">
                        <div class="row fade-in">
                            <div class="col">
                                {{ $slot }}
                            </div>

                            @if (isset($aside))
                                <div class="col-lg-3">
                                    {{ $aside ?? '' }}
                                </div>
                            @endif
                        </div>
                    </div>

                </main>

                <footer class="c-footer">
                    <div class="ml-auto">Powered by&nbsp;<a href="https://coreui.io/">CoreUI</a></div>
                </footer>
            </div>
        </div>
        @include('sweetalert::alert')
        @stack('modals')
        @livewireScripts
        @stack('scripts')
    </body>
</html>
