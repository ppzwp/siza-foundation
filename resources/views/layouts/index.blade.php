<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 mb-0 font-weight-bold">
            {{ $header }}
        </h2>
    </x-slot>

    @if (isset($toolbar_left) OR isset($toolbar_right))
        <x-ui::toolbar class="row justify-content-between mb-5">

            @if (isset($toolbar_left))
                <x-ui::col class="col">
                    {{ $toolbar_left }}
                </x-ui::col>
            @endif

            @if (isset($toolbar_right))
                <x-ui::col class="col text-right">
                    {{ $toolbar_right }}
                </x-ui::col>
            @endif

        </x-ui::toolbar>
    @endif

    <x-ui::row class="justify-content-center">
        <x-ui::col class="col-md-12">
            <x-ui::card>
                @if (isset($filter_bar))
                    <x-ui::card-header>
                        {{ $filter_bar }}
                    </x-ui::card-header>
                @endif
                <x-ui::card-body>
                    {{ $slot }}
                </x-ui::card-body>
            </x-ui::card>
        </x-ui::col>
    </x-ui::row>
</x-app-layout>
