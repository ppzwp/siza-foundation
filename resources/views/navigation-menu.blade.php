<ul class="c-header-nav ml-auto mr-4">
    <!-- Teams Dropdown -->
    @if (Laravel\Jetstream\Jetstream::hasTeamFeatures())
        <x-jet-dropdown id="teamManagementDropdown">
            <x-slot name="trigger">
                {{ Auth::user()->currentTeam->name }}

                <x-ui::icon.chevron-down />
            </x-slot>

            <x-slot name="content">
                <!-- Team Management -->
                <h6 class="dropdown-header">
                    {{ __('Manage Team') }}
                </h6>

                <!-- Team Settings -->
                <x-jet-dropdown-link href="{{ route('teams.show', Auth::user()->currentTeam->id) }}">
                    {{ __('Team Settings') }}
                </x-jet-dropdown-link>

                @can('create', Laravel\Jetstream\Jetstream::newTeamModel())
                    <x-jet-dropdown-link href="{{ route('teams.create') }}">
                        {{ __('Create New Team') }}
                    </x-jet-dropdown-link>
                @endcan

                <hr class="dropdown-divider">

                <!-- Team Switcher -->
                <h6 class="dropdown-header">
                    {{ __('Switch Teams') }}
                </h6>

                @foreach (Auth::user()->allTeams() as $team)
                    <x-jet-switchable-team :team="$team" />
                @endforeach
            </x-slot>
        </x-jet-dropdown>
    @endif

<!-- Authentication Links -->
    @auth
        <x-jet-dropdown id="navbarDropdown">
            <x-slot name="trigger">
                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <div class="c-avatar">
                        <img class="c-avatar-img" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                    </div>
                @else
                    {{ Auth::user()->name }}

                    <x-ui::icon.chevron-down />
                @endif
            </x-slot>

            <x-slot name="content">
                <div class="dropdown-header bg-light py-2">
                    <strong>{{ __('Akaun Anda') }}</strong>
                </div>

                <x-jet-dropdown-link href="{{ config('siza-foundation.url').'/user/profile' }}">
                    {{ __('Maklumat Peribadi') }}
                </x-jet-dropdown-link>

                <x-jet-dropdown-link href="{{ config('siza-foundation.url').'/user/settings' }}">
                    {{ __('Tetapan Akaun') }}
                </x-jet-dropdown-link>

                @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                    <x-jet-dropdown-link href="{{ route('api-tokens.index') }}">
                        {{ __('API Tokens') }}
                    </x-jet-dropdown-link>
                @endif

                <hr class="dropdown-divider">

                <!-- Authentication -->
                <x-jet-dropdown-link href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Log Out') }}
                </x-jet-dropdown-link>
                <form method="POST" id="logout-form" action="{{ route('logout') }}">
                    @csrf
                </form>
            </x-slot>
        </x-jet-dropdown>
    @endauth
</ul>
