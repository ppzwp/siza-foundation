<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Getting Started') }}
        </h2>
    </x-slot>

    <x-slot name="sidebar">
        @include('docs::sidebar')
    </x-slot>

    <x-ui::container>
        <x-ui::row class="justify-content-center">
            <x-ui::col>
                <x-ui::card class="px-5 shadow-sm">
                    <x-ui::card-header>Layout</x-ui::card-header>
                    <x-ui::card-body>
                        Components and options for laying out your SIZA project, including wrapping containers,
                        a powerful grid system, a flexible media object, and responsive utility classes.
                    </x-ui::card-body>
                    <x-ui::card-footer>
                        <x-ui::btn type="button" class="btn btn-success">
                            View Page
                        </x-ui::btn>
                    </x-ui::card-footer>
                </x-ui::card>
            </x-ui::col>

            <x-ui::col>
                <x-ui::card class="shadow-sm">
                    <x-ui::card-header>Layout</x-ui::card-header>
                    <x-ui::card-body>
                        Components and options for laying out your SIZA project, including wrapping containers,
                        a powerful grid system, a flexible media object, and responsive utility classes.
                    </x-ui::card-body>
                    <x-ui::card-footer>
                        Components and options for laying out your SIZA project, including wrapping containers,
                        a powerful grid system, a flexible media object, and responsive utility classes.
                    </x-ui::card-footer>
                </x-ui::card>
            </x-ui::col>
        </x-ui::row>
    </x-ui::container>



    <x-ui::alert type="secondary">
        This is alert primary
    </x-ui::alert>
</x-app-layout>
