<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Getting Started') }}
        </h2>
    </x-slot>

    <x-slot name="sidebar">
        @include('docs::sidebar')
    </x-slot>

    <x-ui::container>
        <x-ui::row class="justify-content-center">
            <x-ui::col>
                <x-ui::card>
                    <x-ui::card-body class="px-5 shadow-sm">
                        <main role="main">
                            <h1>Grid system</h1>
                            <p class="bd-lead">Use our powerful mobile-first flexbox grid to build layouts of all shapes
                                and sizes thanks to a twelve column system, five default responsive tiers, Sass
                                variables and mixins, and dozens of predefined classes.</p>

                            <h2 id="how-it-works"><span class="bd-content-title">How it works<a class="anchorjs-link "
                                                                                                aria-label="Anchor"
                                                                                                data-anchorjs-icon="#"
                                                                                                href="#how-it-works"
                                                                                                style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Bootstrap’s grid system uses a series of containers, rows, and columns to layout and
                                align content. It’s built with <a
                                        href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox">flexbox</a>
                                and is fully responsive. Below is an example and an in-depth look at how the grid comes
                                together.</p>
                            <p><strong>New to or unfamiliar with flexbox?</strong> <a
                                        href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/#flexbox-background">Read
                                    this CSS Tricks flexbox guide</a> for background, terminology, guidelines, and code
                                snippets.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm">
                                            One of three columns
                                        </div>
                                        <div class="col-sm">
                                            One of three columns
                                        </div>
                                        <div class="col-sm">
                                            One of three columns
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p>The above example creates three equal-width columns on small, medium, large, and extra
                                large devices using our predefined grid classes. Those columns are centered in the page
                                with the parent <code>.container</code>.</p>
                            <p>Breaking it down, here’s how it works:</p>
                            <ul>
                                <li>Containers provide a means to center and horizontally pad your site’s contents. Use
                                    <code>.container</code> for a responsive pixel width or
                                    <code>.container-fluid</code> for <code>width: 100%</code> across all viewport and
                                    device sizes.
                                </li>
                                <li>Rows are wrappers for columns. Each column has horizontal <code>padding</code>
                                    (called a gutter) for controlling the space between them. This <code>padding</code>
                                    is then counteracted on the rows with negative margins. This way, all the content in
                                    your columns is visually aligned down the left side.
                                </li>
                                <li>In a grid layout, content must be placed within columns and only columns may be
                                    immediate children of rows.
                                </li>
                                <li>Thanks to flexbox, grid columns without a specified <code>width</code> will
                                    automatically layout as equal width columns. For example, four instances of <code>.col-sm</code>
                                    will each automatically be 25% wide from the small breakpoint and up. See the <a
                                            href="#auto-layout-columns">auto-layout columns</a> section for more
                                    examples.
                                </li>
                                <li>Column classes indicate the number of columns you’d like to use out of the possible
                                    12 per row. So, if you want three equal-width columns across, you can use <code>.col-4</code>.
                                </li>
                                <li>Column <code>width</code>s are set in percentages, so they’re always fluid and sized
                                    relative to their parent element.
                                </li>
                                <li>Columns have horizontal <code>padding</code> to create the gutters between
                                    individual columns, however, you can remove the <code>margin</code> from rows and
                                    <code>padding</code> from columns with <code>.no-gutters</code> on the
                                    <code>.row</code>.
                                </li>
                                <li>To make the grid responsive, there are five grid breakpoints, one for each <a
                                            href="/docs/4.6/layout/overview/#responsive-breakpoints">responsive
                                        breakpoint</a>: all breakpoints (extra small), small, medium, large, and extra
                                    large.
                                </li>
                                <li>Grid breakpoints are based on minimum width media queries, meaning <strong>they
                                        apply to that one breakpoint and all those above it</strong> (e.g., <code>.col-sm-4</code>
                                    applies to small, medium, large, and extra large devices, but not the first
                                    <code>xs</code> breakpoint).
                                </li>
                                <li>You can use predefined grid classes (like <code>.col-4</code>) or <a
                                            href="#sass-mixins">Sass mixins</a> for more semantic markup.
                                </li>
                            </ul>
                            <p>Be aware of the limitations and <a href="https://github.com/philipwalton/flexbugs">bugs
                                    around flexbox</a>, like the <a
                                        href="https://github.com/philipwalton/flexbugs#flexbug-9">inability to use some
                                    HTML elements as flex containers</a>.</p>
                            <h2 id="grid-options"><span class="bd-content-title">Grid options<a class="anchorjs-link "
                                                                                                aria-label="Anchor"
                                                                                                data-anchorjs-icon="#"
                                                                                                href="#grid-options"
                                                                                                style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>While Bootstrap uses <code>em</code>s or <code>rem</code>s for defining most sizes,
                                <code>px</code>s are used for grid breakpoints and container widths. This is because the
                                viewport width is in pixels and does not change with the <a
                                        href="https://drafts.csswg.org/mediaqueries-3/#units">font size</a>.</p>
                            <p>See how aspects of the Bootstrap grid system work across multiple devices with a handy
                                table.</p>
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">
                                        Extra small<br>
                                        <small>&lt;576px</small>
                                    </th>
                                    <th class="text-center">
                                        Small<br>
                                        <small>≥576px</small>
                                    </th>
                                    <th class="text-center">
                                        Medium<br>
                                        <small>≥768px</small>
                                    </th>
                                    <th class="text-center">
                                        Large<br>
                                        <small>≥992px</small>
                                    </th>
                                    <th class="text-center">
                                        Extra large<br>
                                        <small>≥1200px</small>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">Max container width</th>
                                    <td>None (auto)</td>
                                    <td>540px</td>
                                    <td>720px</td>
                                    <td>960px</td>
                                    <td>1140px</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Class prefix</th>
                                    <td><code>.col-</code></td>
                                    <td><code>.col-sm-</code></td>
                                    <td><code>.col-md-</code></td>
                                    <td><code>.col-lg-</code></td>
                                    <td><code>.col-xl-</code></td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row"># of columns</th>
                                    <td colspan="5">12</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Gutter width</th>
                                    <td colspan="5">30px (15px on each side of a column)</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Nestable</th>
                                    <td colspan="5">Yes</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Column ordering</th>
                                    <td colspan="5">Yes</td>
                                </tr>
                                </tbody>
                            </table>
                            <h2 id="auto-layout-columns"><span class="bd-content-title">Auto-layout columns<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#auto-layout-columns" style="padding-left: 0.375em;"></a></span></h2>
                            <p>Utilize breakpoint-specific column classes for easy column sizing without an explicit
                                numbered class like <code>.col-sm-6</code>.</p>
                            <h3 id="equal-width"><span class="bd-content-title">Equal-width<a class="anchorjs-link "
                                                                                              aria-label="Anchor"
                                                                                              data-anchorjs-icon="#"
                                                                                              href="#equal-width"
                                                                                              style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>For example, here are two grid layouts that apply to every device and viewport, from
                                <code>xs</code> to <code>xl</code>. Add any number of unit-less classes for each
                                breakpoint you need and every column will be the same width.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            1 of 2
                                        </div>
                                        <div class="col">
                                            2 of 2
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            1 of 3
                                        </div>
                                        <div class="col">
                                            2 of 3
                                        </div>
                                        <div class="col">
                                            3 of 3
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      1 of 2
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      2 of 2
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      1 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      2 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      3 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="equal-width-multi-line"><span class="bd-content-title">Equal-width multi-line<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#equal-width-multi-line" style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Create equal-width columns that span multiple lines by inserting a <code>.w-100</code>
                                where you want the columns to break to a new line. Make the breaks responsive by mixing
                                <code>.w-100</code> with some <a href="/docs/4.6/utilities/display/">responsive display
                                    utilities</a>.</p>
                            <p>There was a <a href="https://github.com/philipwalton/flexbugs#flexbug-11">Safari flexbox
                                    bug</a> that prevented this from working without an explicit <code>flex-basis</code>
                                or <code>border</code>. There are workarounds for older browser versions, but they
                                shouldn’t be necessary if your target browsers don’t fall into the buggy versions.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">col</div>
                                        <div class="col">col</div>
                                        <div class="w-100"></div>
                                        <div class="col">col</div>
                                        <div class="col">col</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"w-100"</span><span class="p">&gt;&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="setting-one-column-width"><span class="bd-content-title">Setting one column width<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#setting-one-column-width" style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Auto-layout for flexbox grid columns also means you can set the width of one column and
                                have the sibling columns automatically resize around it. You may use predefined grid
                                classes (as shown below), grid mixins, or inline widths. Note that the other columns
                                will resize no matter the width of the center column.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            1 of 3
                                        </div>
                                        <div class="col-6">
                                            2 of 3 (wider)
                                        </div>
                                        <div class="col">
                                            3 of 3
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            1 of 3
                                        </div>
                                        <div class="col-5">
                                            2 of 3 (wider)
                                        </div>
                                        <div class="col">
                                            3 of 3
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      1 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6"</span><span class="p">&gt;</span>
      2 of 3 (wider)
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      3 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      1 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-5"</span><span class="p">&gt;</span>
      2 of 3 (wider)
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      3 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="variable-width-content"><span class="bd-content-title">Variable width content<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#variable-width-content" style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Use <code>col-{breakpoint}-auto</code> classes to size columns based on the natural width
                                of their content.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row justify-content-md-center">
                                        <div class="col col-lg-2">
                                            1 of 3
                                        </div>
                                        <div class="col-md-auto">
                                            Variable width content
                                        </div>
                                        <div class="col col-lg-2">
                                            3 of 3
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            1 of 3
                                        </div>
                                        <div class="col-md-auto">
                                            Variable width content
                                        </div>
                                        <div class="col col-lg-2">
                                            3 of 3
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-md-center"</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col col-lg-2"</span><span class="p">&gt;</span>
      1 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-auto"</span><span class="p">&gt;</span>
      Variable width content
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col col-lg-2"</span><span class="p">&gt;</span>
      3 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      1 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-auto"</span><span class="p">&gt;</span>
      Variable width content
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col col-lg-2"</span><span class="p">&gt;</span>
      3 of 3
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="responsive-classes"><span class="bd-content-title">Responsive classes<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#responsive-classes" style="padding-left: 0.375em;"></a></span></h2>
                            <p>Bootstrap’s grid includes five tiers of predefined classes for building complex
                                responsive layouts. Customize the size of your columns on extra small, small, medium,
                                large, or extra large devices however you see fit.</p>
                            <h3 id="all-breakpoints"><span class="bd-content-title">All breakpoints<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#all-breakpoints" style="padding-left: 0.375em;"></a></span></h3>
                            <p>For grids that are the same from the smallest of devices to the largest, use the <code>.col</code>
                                and <code>.col-*</code> classes. Specify a numbered class when you need a particularly
                                sized column; otherwise, feel free to stick to <code>.col</code>.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">col</div>
                                        <div class="col">col</div>
                                        <div class="col">col</div>
                                        <div class="col">col</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-8">col-8</div>
                                        <div class="col-4">col-4</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>col<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-8"</span><span class="p">&gt;</span>col-8<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>col-4<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="stacked-to-horizontal"><span class="bd-content-title">Stacked to horizontal<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#stacked-to-horizontal" style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Using a single set of <code>.col-sm-*</code> classes, you can create a basic grid system
                                that starts out stacked and becomes horizontal at the small breakpoint (<code>sm</code>).
                            </p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-8">col-sm-8</div>
                                        <div class="col-sm-4">col-sm-4</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm">col-sm</div>
                                        <div class="col-sm">col-sm</div>
                                        <div class="col-sm">col-sm</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-8"</span><span class="p">&gt;</span>col-sm-8<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-4"</span><span class="p">&gt;</span>col-sm-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>col-sm<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>col-sm<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm"</span><span class="p">&gt;</span>col-sm<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="mix-and-match"><span class="bd-content-title">Mix and match<a class="anchorjs-link "
                                                                                                  aria-label="Anchor"
                                                                                                  data-anchorjs-icon="#"
                                                                                                  href="#mix-and-match"
                                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Don’t want your columns to simply stack in some grid tiers? Use a combination of
                                different classes for each tier as needed. See the example below for a better idea of
                                how it all works.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <!-- Stack the columns on mobile by making one full-width and the other half-width -->
                                    <div class="row">
                                        <div class="col-md-8">.col-md-8</div>
                                        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                                    </div>

                                    <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
                                    <div class="row">
                                        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                                        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                                        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                                    </div>

                                    <!-- Columns are always 50% wide, on mobile and desktop -->
                                    <div class="row">
                                        <div class="col-6">.col-6</div>
                                        <div class="col-6">.col-6</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="c">&lt;!-- Stack the columns on mobile by making one full-width and the other half-width --&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-8"</span><span class="p">&gt;</span>.col-md-8<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-md-4"</span><span class="p">&gt;</span>.col-6 .col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>

  <span class="c">&lt;!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop --&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-md-4"</span><span class="p">&gt;</span>.col-6 .col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-md-4"</span><span class="p">&gt;</span>.col-6 .col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-md-4"</span><span class="p">&gt;</span>.col-6 .col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>

  <span class="c">&lt;!-- Columns are always 50% wide, on mobile and desktop --&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6"</span><span class="p">&gt;</span>.col-6<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6"</span><span class="p">&gt;</span>.col-6<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="gutters"><span class="bd-content-title">Gutters<a class="anchorjs-link "
                                                                                      aria-label="Anchor"
                                                                                      data-anchorjs-icon="#"
                                                                                      href="#gutters"
                                                                                      style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Gutters can be responsively adjusted by breakpoint-specific padding and negative margin
                                utility classes. To change the gutters in a given row, pair a negative margin utility on
                                the <code>.row</code> and matching padding utilities on the <code>.col</code>s. The
                                <code>.container</code> or <code>.container-fluid</code> parent may need to be adjusted
                                too to avoid unwanted overflow, using again matching padding utility.</p>
                            <p>Here’s an example of customizing the Bootstrap grid at the large (<code>lg</code>)
                                breakpoint and above. We’ve increased the <code>.col</code> padding with
                                <code>.px-lg-5</code>, counteracted that with <code>.mx-lg-n5</code> on the parent
                                <code>.row</code> and then adjusted the <code>.container</code> wrapper with <code>.px-lg-5</code>.
                            </p>
                            <div class="bd-example">
                                <div class="container px-lg-5">
                                    <div class="row mx-lg-n5">
                                        <div class="col py-3 px-lg-5 border bg-light">Custom column padding</div>
                                        <div class="col py-3 px-lg-5 border bg-light">Custom column padding</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container px-lg-5"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row mx-lg-n5"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col py-3 px-lg-5 border bg-light"</span><span
                                                class="p">&gt;</span>Custom column padding<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col py-3 px-lg-5 border bg-light"</span><span
                                                class="p">&gt;</span>Custom column padding<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="row-columns"><span class="bd-content-title">Row columns<a class="anchorjs-link "
                                                                                              aria-label="Anchor"
                                                                                              data-anchorjs-icon="#"
                                                                                              href="#row-columns"
                                                                                              style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Use the responsive <code>.row-cols-*</code> classes to quickly set the number of columns
                                that best render your content and layout. Whereas normal <code>.col-*</code> classes
                                apply to the individual columns (e.g., <code>.col-md-4</code>), the row columns classes
                                are set on the parent <code>.row</code> as a shortcut.</p>
                            <p>Use these row columns classes to quickly create basic grid layouts or to control your
                                card layouts.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row row-cols-2">
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row row-cols-2"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row row-cols-3">
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row row-cols-3"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row row-cols-4">
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row row-cols-4"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row row-cols-4">
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col-6">Column</div>
                                        <div class="col">Column</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row row-cols-4"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6"</span><span class="p">&gt;</span>Column<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                        <div class="col">Column</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row row-cols-1 row-cols-sm-2 row-cols-md-4"</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>Column<span class="p">&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p>You can also use the accompanying Sass mixin, <code>row-cols()</code>:</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span
                                                class="nc">.element</span> <span class="p">{</span>
  <span class="c1">// Three columns to start
</span><span class="c1"></span>  <span class="k">@@include</span><span class="nd"> row-cols</span><span
                                                class="p">(</span><span class="mi">3</span><span class="p">);</span>

  <span class="c1">// Five columns from medium breakpoint up
</span><span class="c1"></span>  <span class="k">@@include</span><span class="nd"> media-breakpoint-up</span><span
                                                class="p">(</span><span class="n">md</span><span
                                                class="p">)</span> <span class="p">{</span>
    <span class="k">@@include</span><span class="nd"> row-cols</span><span class="p">(</span><span
                                                class="mi">5</span><span class="p">);</span>
  <span class="p">}</span>
<span class="p">}</span>
</code></pre>
                            </div>
                            <h2 id="alignment"><span class="bd-content-title">Alignment<a class="anchorjs-link "
                                                                                          aria-label="Anchor"
                                                                                          data-anchorjs-icon="#"
                                                                                          href="#alignment"
                                                                                          style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Use flexbox alignment utilities to vertically and horizontally align columns. <strong>Internet
                                    Explorer 10-11 do not support vertical alignment of flex items when the flex
                                    container has a <code>min-height</code> as shown below.</strong> <a
                                        href="https://github.com/philipwalton/flexbugs#flexbug-3">See Flexbugs #3 for
                                    more details.</a></p>
                            <h3 id="vertical-alignment"><span class="bd-content-title">Vertical alignment<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#vertical-alignment" style="padding-left: 0.375em;"></a></span></h3>
                            <div class="bd-example bd-example-row bd-example-row-flex-cols">
                                <div class="container">
                                    <div class="row align-items-start">
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                    </div>
                                    <div class="row align-items-center">
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                    </div>
                                    <div class="row align-items-end">
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                        <div class="col">
                                            One of three columns
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row align-items-start"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row align-items-center"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row align-items-end"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example bd-example-row bd-example-row-flex-cols">
                                <div class="container">
                                    <div class="row">
                                        <div class="col align-self-start">
                                            One of three columns
                                        </div>
                                        <div class="col align-self-center">
                                            One of three columns
                                        </div>
                                        <div class="col align-self-end">
                                            One of three columns
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col align-self-start"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col align-self-center"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col align-self-end"</span><span class="p">&gt;</span>
      One of three columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="horizontal-alignment"><span class="bd-content-title">Horizontal alignment<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#horizontal-alignment" style="padding-left: 0.375em;"></a></span></h3>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row justify-content-start">
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                    </div>
                                    <div class="row justify-content-around">
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                    </div>
                                    <div class="row justify-content-between">
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                        <div class="col-4">
                                            One of two columns
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-start"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-center"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-end"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-around"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row justify-content-between"</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>
      One of two columns
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="no-gutters"><span class="bd-content-title">No gutters<a class="anchorjs-link "
                                                                                            aria-label="Anchor"
                                                                                            data-anchorjs-icon="#"
                                                                                            href="#no-gutters"
                                                                                            style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>The gutters between columns in our predefined grid classes can be removed with <code>.no-gutters</code>.
                                This removes the negative <code>margin</code>s from <code>.row</code> and the horizontal
                                <code>padding</code> from all immediate children columns.</p>
                            <p>Here’s the source code for creating these styles. Note that column overrides are scoped
                                to only the first children columns and are targeted via <a
                                        href="https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors">attribute
                                    selector</a>. While this generates a more specific selector, column padding can
                                still be further customized with <a href="/docs/4.6/utilities/spacing/">spacing
                                    utilities</a>.</p>
                            <p><strong>Need an edge-to-edge design?</strong> Drop the parent <code>.container</code> or
                                <code>.container-fluid</code>.</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="nc">.no-gutters</span> <span
                                                class="p">{</span>
  <span class="nt">margin-right</span><span class="nd">:</span> <span class="nt">0</span><span class="p">;</span>
  <span class="nt">margin-left</span><span class="nd">:</span> <span class="nt">0</span><span class="p">;</span>

  <span class="o">&gt;</span> <span class="nc">.col</span><span class="o">,</span>
  <span class="o">&gt;</span> <span class="o">[</span><span class="nt">class</span><span class="o">*=</span><span
                                                class="s2">"col-"</span><span class="o">]</span> <span
                                                class="p">{</span>
    <span class="nt">padding-right</span><span class="nd">:</span> <span class="nt">0</span><span class="p">;</span>
    <span class="nt">padding-left</span><span class="nd">:</span> <span class="nt">0</span><span class="p">;</span>
  <span class="p">}</span>
<span class="p">}</span>
</code></pre>
                            </div>
                            <p>In practice, here’s how it looks. Note you can continue to use this with all other
                                predefined grid classes (including column widths, responsive tiers, reorders, and
                                more).</p>
                            <div class="bd-example bd-example-row">
                                <div class="row no-gutters">
                                    <div class="col-sm-6 col-md-8">.col-sm-6 .col-md-8</div>
                                    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"row no-gutters"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-6 col-md-8"</span><span class="p">&gt;</span>.col-sm-6 .col-md-8<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-md-4"</span><span class="p">&gt;</span>.col-6 .col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="column-wrapping"><span class="bd-content-title">Column wrapping<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#column-wrapping" style="padding-left: 0.375em;"></a></span></h3>
                            <p>If more than 12 columns are placed within a single row, each group of extra columns will,
                                as one unit, wrap onto a new line.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-9">.col-9</div>
                                        <div class="col-4">.col-4<br>Since 9 + 4 = 13 &gt; 12, this 4-column-wide div
                                            gets wrapped onto a new line as one contiguous unit.
                                        </div>
                                        <div class="col-6">.col-6<br>Subsequent columns continue along the new line.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-9"</span><span class="p">&gt;</span>.col-9<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-4"</span><span class="p">&gt;</span>.col-4<span
                                                class="p">&lt;</span><span class="nt">br</span><span
                                                class="p">&gt;</span>Since 9 + 4 = 13 <span class="ni">&amp;gt;</span> 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6"</span><span class="p">&gt;</span>.col-6<span
                                                class="p">&lt;</span><span class="nt">br</span><span
                                                class="p">&gt;</span>Subsequent columns continue along the new line.<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="column-breaks"><span class="bd-content-title">Column breaks<a class="anchorjs-link "
                                                                                                  aria-label="Anchor"
                                                                                                  data-anchorjs-icon="#"
                                                                                                  href="#column-breaks"
                                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Breaking columns to a new line in flexbox requires a small hack: add an element with
                                <code>width: 100%</code> wherever you want to wrap your columns to a new line. Normally
                                this is accomplished with multiple <code>.row</code>s, but not every implementation
                                method can account for this.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
                                        <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>

                                        <!-- Force next columns to break to new line -->
                                        <div class="w-100"></div>

                                        <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
                                        <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-3"</span><span class="p">&gt;</span>.col-6 .col-sm-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-3"</span><span class="p">&gt;</span>.col-6 .col-sm-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>

    <span class="c">&lt;!-- Force next columns to break to new line --&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"w-100"</span><span class="p">&gt;&lt;/</span><span
                                                class="nt">div</span><span class="p">&gt;</span>

    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-3"</span><span class="p">&gt;</span>.col-6 .col-sm-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-3"</span><span class="p">&gt;</span>.col-6 .col-sm-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p>You may also apply this break at specific breakpoints with our <a
                                        href="/docs/4.6/utilities/display/">responsive display utilities</a>.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6 col-sm-4">.col-6 .col-sm-4</div>
                                        <div class="col-6 col-sm-4">.col-6 .col-sm-4</div>

                                        <!-- Force next columns to break to new line at md breakpoint and up -->
                                        <div class="w-100 d-none d-md-block"></div>

                                        <div class="col-6 col-sm-4">.col-6 .col-sm-4</div>
                                        <div class="col-6 col-sm-4">.col-6 .col-sm-4</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-4"</span><span class="p">&gt;</span>.col-6 .col-sm-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-4"</span><span class="p">&gt;</span>.col-6 .col-sm-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>

    <span class="c">&lt;!-- Force next columns to break to new line at md breakpoint and up --&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"w-100 d-none d-md-block"</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>

    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-4"</span><span class="p">&gt;</span>.col-6 .col-sm-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-6 col-sm-4"</span><span class="p">&gt;</span>.col-6 .col-sm-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="reordering"><span class="bd-content-title">Reordering<a class="anchorjs-link "
                                                                                            aria-label="Anchor"
                                                                                            data-anchorjs-icon="#"
                                                                                            href="#reordering"
                                                                                            style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <h3 id="order-classes"><span class="bd-content-title">Order classes<a class="anchorjs-link "
                                                                                                  aria-label="Anchor"
                                                                                                  data-anchorjs-icon="#"
                                                                                                  href="#order-classes"
                                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Use <code>.order-</code> classes for controlling the <strong>visual order</strong> of
                                your content. These classes are responsive, so you can set the <code>order</code> by
                                breakpoint (e.g., <code>.order-1.order-md-2</code>). Includes support for <code>1</code>
                                through <code>12</code> across all five grid tiers.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            First in DOM, no order applied
                                        </div>
                                        <div class="col order-12">
                                            Second in DOM, with a larger order
                                        </div>
                                        <div class="col order-1">
                                            Third in DOM, with an order of 1
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      First in DOM, no order applied
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col order-12"</span><span class="p">&gt;</span>
      Second in DOM, with a larger order
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col order-1"</span><span class="p">&gt;</span>
      Third in DOM, with an order of 1
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p>There are also responsive <code>.order-first</code> and <code>.order-last</code> classes
                                that change the <code>order</code> of an element by applying <code>order: -1</code> and
                                <code>order: 13</code> (<code>order: $columns + 1</code>), respectively. These classes
                                can also be intermixed with the numbered <code>.order-*</code> classes as needed.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col order-last">
                                            First in DOM, ordered last
                                        </div>
                                        <div class="col">
                                            Second in DOM, unordered
                                        </div>
                                        <div class="col order-first">
                                            Third in DOM, ordered first
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col order-last"</span><span class="p">&gt;</span>
      First in DOM, ordered last
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col"</span><span class="p">&gt;</span>
      Second in DOM, unordered
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col order-first"</span><span class="p">&gt;</span>
      Third in DOM, ordered first
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="offsetting-columns"><span class="bd-content-title">Offsetting columns<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#offsetting-columns" style="padding-left: 0.375em;"></a></span></h3>
                            <p>You can offset grid columns in two ways: our responsive <code>.offset-</code> grid
                                classes and our <a href="/docs/4.6/utilities/spacing/">margin utilities</a>. Grid
                                classes are sized to match columns while margins are more useful for quick layouts where
                                the width of the offset is variable.</p>
                            <h4 id="offset-classes"><span class="bd-content-title">Offset classes<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#offset-classes" style="padding-left: 0.375em;"></a></span></h4>
                            <p>Move columns to the right using <code>.offset-md-*</code> classes. These classes increase
                                the left margin of a column by <code>*</code> columns. For example,
                                <code>.offset-md-4</code> moves <code>.col-md-4</code> over four columns.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">.col-md-4</div>
                                        <div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
                                        <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 offset-md-3">.col-md-6 .offset-md-3</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-4"</span><span class="p">&gt;</span>.col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-4 offset-md-4"</span><span class="p">&gt;</span>.col-md-4 .offset-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-3 offset-md-3"</span><span class="p">&gt;</span>.col-md-3 .offset-md-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-3 offset-md-3"</span><span class="p">&gt;</span>.col-md-3 .offset-md-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-6 offset-md-3"</span><span class="p">&gt;</span>.col-md-6 .offset-md-3<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p>In addition to column clearing at responsive breakpoints, you may need to reset offsets.
                                See this in action in <a href="/docs/4.6/examples/grid/">the grid example</a>.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-6">.col-sm-5 .col-md-6</div>
                                        <div class="col-sm-5 offset-sm-2 col-md-6 offset-md-0">.col-sm-5 .offset-sm-2
                                            .col-md-6 .offset-md-0
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-5 col-lg-6">.col-sm-6 .col-md-5 .col-lg-6</div>
                                        <div class="col-sm-6 col-md-5 offset-md-2 col-lg-6 offset-lg-0">.col-sm-6
                                            .col-md-5 .offset-md-2 .col-lg-6 .offset-lg-0
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-5 col-md-6"</span><span class="p">&gt;</span>.col-sm-5 .col-md-6<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-5 offset-sm-2 col-md-6 offset-md-0"</span><span
                                                class="p">&gt;</span>.col-sm-5 .offset-sm-2 .col-md-6 .offset-md-0<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-6 col-md-5 col-lg-6"</span><span class="p">&gt;</span>.col-sm-6 .col-md-5 .col-lg-6<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-6 col-md-5 offset-md-2 col-lg-6 offset-lg-0"</span><span
                                                class="p">&gt;</span>.col-sm-6 .col-md-5 .offset-md-2 .col-lg-6 .offset-lg-0<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h4 id="margin-utilities"><span class="bd-content-title">Margin utilities<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#margin-utilities" style="padding-left: 0.375em;"></a></span></h4>
                            <p>With the move to flexbox in v4, you can use margin utilities like <code>.mr-auto</code>
                                to force sibling columns away from one another.</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">.col-md-4</div>
                                        <div class="col-md-4 ml-auto">.col-md-4 .ml-auto</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto</div>
                                        <div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-auto mr-auto">.col-auto .mr-auto</div>
                                        <div class="col-auto">.col-auto</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-4"</span><span class="p">&gt;</span>.col-md-4<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-4 ml-auto"</span><span class="p">&gt;</span>.col-md-4 .ml-auto<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-3 ml-md-auto"</span><span class="p">&gt;</span>.col-md-3 .ml-md-auto<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-md-3 ml-md-auto"</span><span class="p">&gt;</span>.col-md-3 .ml-md-auto<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-auto mr-auto"</span><span class="p">&gt;</span>.col-auto .mr-auto<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-auto"</span><span class="p">&gt;</span>.col-auto<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="nesting"><span class="bd-content-title">Nesting<a class="anchorjs-link "
                                                                                      aria-label="Anchor"
                                                                                      data-anchorjs-icon="#"
                                                                                      href="#nesting"
                                                                                      style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>To nest your content with the default grid, add a new <code>.row</code> and set of <code>.col-sm-*</code>
                                columns within an existing <code>.col-sm-*</code> column. Nested rows should include a
                                set of columns that add up to 12 or fewer (it is not required that you use all 12
                                available columns).</p>
                            <div class="bd-example bd-example-row">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            Level 1: .col-sm-9
                                            <div class="row">
                                                <div class="col-8 col-sm-6">
                                                    Level 2: .col-8 .col-sm-6
                                                </div>
                                                <div class="col-4 col-sm-6">
                                                    Level 2: .col-4 .col-sm-6
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>
      Level 1: .col-sm-9
      <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
        <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span
                                                class="o">=</span><span class="s">"col-8 col-sm-6"</span><span
                                                class="p">&gt;</span>
          Level 2: .col-8 .col-sm-6
        <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
        <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span
                                                class="o">=</span><span class="s">"col-4 col-sm-6"</span><span
                                                class="p">&gt;</span>
          Level 2: .col-4 .col-sm-6
        <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
      <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
    <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="sass-mixins"><span class="bd-content-title">Sass mixins<a class="anchorjs-link "
                                                                                              aria-label="Anchor"
                                                                                              data-anchorjs-icon="#"
                                                                                              href="#sass-mixins"
                                                                                              style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>When using Bootstrap’s source Sass files, you have the option of using Sass variables and
                                mixins to create custom, semantic, and responsive page layouts. Our predefined grid
                                classes use these same variables and mixins to provide a whole suite of ready-to-use
                                classes for fast responsive layouts.</p>
                            <h3 id="variables"><span class="bd-content-title">Variables<a class="anchorjs-link "
                                                                                          aria-label="Anchor"
                                                                                          data-anchorjs-icon="#"
                                                                                          href="#variables"
                                                                                          style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Variables and maps determine the number of columns, the gutter width, and the media query
                                point at which to begin floating columns. We use these to generate the predefined grid
                                classes documented above, as well as for the custom mixins listed below.</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="nv">$grid-columns</span><span
                                                class="o">:</span>      <span class="mi">12</span><span
                                                class="p">;</span>
<span class="nv">$grid-gutter-width</span><span class="o">:</span> <span class="mi">30</span><span
                                                class="kt">px</span><span class="p">;</span>

<span class="nv">$grid-breakpoints</span><span class="o">:</span> <span class="p">(</span>
  <span class="c1">// Extra small screen / phone</span>
  <span class="n">xs</span><span class="o">:</span> <span class="mi">0</span><span class="o">,</span>
  <span class="c1">// Small screen / phone</span>
  <span class="n">sm</span><span class="o">:</span> <span class="mi">576</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="c1">// Medium screen / tablet</span>
  <span class="n">md</span><span class="o">:</span> <span class="mi">768</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="c1">// Large screen / desktop</span>
  <span class="n">lg</span><span class="o">:</span> <span class="mi">992</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="c1">// Extra large screen / wide desktop</span>
  <span class="n">xl</span><span class="o">:</span> <span class="mi">1200</span><span class="kt">px</span>
<span class="p">);</span>

<span class="nv">$container-max-widths</span><span class="o">:</span> <span class="p">(</span>
  <span class="n">sm</span><span class="o">:</span> <span class="mi">540</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">md</span><span class="o">:</span> <span class="mi">720</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">lg</span><span class="o">:</span> <span class="mi">960</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">xl</span><span class="o">:</span> <span class="mi">1140</span><span class="kt">px</span>
<span class="p">);</span>
</code></pre>
                            </div>
                            <h3 id="mixins"><span class="bd-content-title">Mixins<a class="anchorjs-link "
                                                                                    aria-label="Anchor"
                                                                                    data-anchorjs-icon="#"
                                                                                    href="#mixins"
                                                                                    style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Mixins are used in conjunction with the grid variables to generate semantic CSS for
                                individual grid columns.</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="c1">// Creates a wrapper for a series of columns
</span><span class="c1"></span><span class="k">@@include</span><span class="nd"> make-row</span><span
                                                class="p">();</span>

<span class="c1">// Make the element grid-ready (applying everything but the width)
</span><span class="c1"></span><span class="k">@@include</span><span class="nd"> make-col-ready</span><span
                                                class="p">();</span>
<span class="k">@@include</span><span class="nd"> make-col</span><span class="p">(</span><span
                                                class="nv">$size</span><span class="o">,</span> <span class="nv">$columns</span><span
                                                class="o">:</span> <span class="nv">$grid-columns</span><span class="p">);</span>

<span class="c1">// Get fancy by offsetting, or changing the sort order
</span><span class="c1"></span><span class="k">@@include</span><span class="nd"> make-col-offset</span><span
                                                class="p">(</span><span class="nv">$size</span><span class="o">,</span> <span
                                                class="nv">$columns</span><span class="o">:</span> <span class="nv">$grid-columns</span><span
                                                class="p">);</span>
</code></pre>
                            </div>
                            <h3 id="example-usage"><span class="bd-content-title">Example usage<a class="anchorjs-link "
                                                                                                  aria-label="Anchor"
                                                                                                  data-anchorjs-icon="#"
                                                                                                  href="#example-usage"
                                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>You can modify the variables to your own custom values, or just use the mixins with their
                                default values. Here’s an example of using the default settings to create a two-column
                                layout with a gap between.</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="nc">.example-container</span> <span
                                                class="p">{</span>
  <span class="k">@@include</span><span class="nd"> make-container</span><span class="p">();</span>
  <span class="c1">// Make sure to define this width after the mixin to override
</span><span class="c1"></span>  <span class="c1">// `width: 100%` generated by `make-container()`
</span><span class="c1"></span>  <span class="nt">width</span><span class="nd">:</span> <span
                                                class="nt">800px</span><span class="p">;</span>
<span class="p">}</span>

<span class="nc">.example-row</span> <span class="p">{</span>
  <span class="k">@@include</span><span class="nd"> make-row</span><span class="p">();</span>
<span class="p">}</span>

<span class="nc">.example-content-main</span> <span class="p">{</span>
  <span class="k">@@include</span><span class="nd"> make-col-ready</span><span class="p">();</span>

  <span class="k">@@include</span><span class="nd"> media-breakpoint-up</span><span class="p">(</span><span
                                                class="n">sm</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">@@include</span><span class="nd"> make-col</span><span class="p">(</span><span
                                                class="mi">6</span><span class="p">);</span>
  <span class="p">}</span>
  <span class="k">@@include</span><span class="nd"> media-breakpoint-up</span><span class="p">(</span><span
                                                class="n">lg</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">@@include</span><span class="nd"> make-col</span><span class="p">(</span><span
                                                class="mi">8</span><span class="p">);</span>
  <span class="p">}</span>
<span class="p">}</span>

<span class="nc">.example-content-secondary</span> <span class="p">{</span>
  <span class="k">@@include</span><span class="nd"> make-col-ready</span><span class="p">();</span>

  <span class="k">@@include</span><span class="nd"> media-breakpoint-up</span><span class="p">(</span><span
                                                class="n">sm</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">@@include</span><span class="nd"> make-col</span><span class="p">(</span><span
                                                class="mi">6</span><span class="p">);</span>
  <span class="p">}</span>
  <span class="k">@@include</span><span class="nd"> media-breakpoint-up</span><span class="p">(</span><span
                                                class="n">lg</span><span class="p">)</span> <span class="p">{</span>
    <span class="k">@@include</span><span class="nd"> make-col</span><span class="p">(</span><span
                                                class="mi">4</span><span class="p">);</span>
  <span class="p">}</span>
<span class="p">}</span>
</code></pre>
                            </div>
                            <div class="bd-example">
                                <div class="example-container">
                                    <div class="example-row">
                                        <div class="example-content-main">Main content</div>
                                        <div class="example-content-secondary">Secondary content</div>
                                    </div>
                                </div>
                            </div>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-html"
                                                                             data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">div</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"example-container"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"example-row"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"example-content-main"</span><span class="p">&gt;</span>Main content<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">div</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"example-content-secondary"</span><span class="p">&gt;</span>Secondary content<span
                                                class="p">&lt;/</span><span class="nt">div</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">div</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="customizing-the-grid"><span class="bd-content-title">Customizing the grid<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#customizing-the-grid" style="padding-left: 0.375em;"></a></span></h2>
                            <p>Using our built-in grid Sass variables and maps, it’s possible to completely customize
                                the predefined grid classes. Change the number of tiers, the media query dimensions, and
                                the container widths—then recompile.</p>
                            <h3 id="columns-and-gutters"><span class="bd-content-title">Columns and gutters<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#columns-and-gutters" style="padding-left: 0.375em;"></a></span></h3>
                            <p>The number of grid columns can be modified via Sass variables. <code>$grid-columns</code>
                                is used to generate the widths (in percent) of each individual column while <code>$grid-gutter-width</code>
                                sets the width for the column gutters.</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="nv">$grid-columns</span><span
                                                class="o">:</span> <span class="mi">12</span> <span
                                                class="nv">!default</span><span class="p">;</span>
<span class="nv">$grid-gutter-width</span><span class="o">:</span> <span class="mi">30</span><span class="kt">px</span> <span
                                                class="nv">!default</span><span class="p">;</span>
</code></pre>
                            </div>
                            <h3 id="grid-tiers"><span class="bd-content-title">Grid tiers<a class="anchorjs-link "
                                                                                            aria-label="Anchor"
                                                                                            data-anchorjs-icon="#"
                                                                                            href="#grid-tiers"
                                                                                            style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Moving beyond the columns themselves, you may also customize the number of grid tiers. If
                                you wanted just four grid tiers, you’d update the <code>$grid-breakpoints</code> and
                                <code>$container-max-widths</code> to something like this:</p>
                            <div class="bd-clipboard">
                                <button type="button" class="btn-clipboard" title=""
                                        data-original-title="Copy to clipboard">Copy
                                </button>
                            </div>
                            <div class="highlight"><pre class="chroma"><code class="language-scss"
                                                                             data-lang="scss"><span class="nv">$grid-breakpoints</span><span
                                                class="o">:</span> <span class="p">(</span>
  <span class="n">xs</span><span class="o">:</span> <span class="mi">0</span><span class="o">,</span>
  <span class="n">sm</span><span class="o">:</span> <span class="mi">480</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">md</span><span class="o">:</span> <span class="mi">768</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">lg</span><span class="o">:</span> <span class="mi">1024</span><span class="kt">px</span>
<span class="p">);</span>

<span class="nv">$container-max-widths</span><span class="o">:</span> <span class="p">(</span>
  <span class="n">sm</span><span class="o">:</span> <span class="mi">420</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">md</span><span class="o">:</span> <span class="mi">720</span><span class="kt">px</span><span
                                                class="o">,</span>
  <span class="n">lg</span><span class="o">:</span> <span class="mi">960</span><span class="kt">px</span>
<span class="p">);</span>
</code></pre>
                            </div>
                            <p>When making any changes to the Sass variables or maps, you’ll need to save your changes
                                and recompile. Doing so will output a brand new set of predefined grid classes for
                                column widths, offsets, and ordering. Responsive visibility utilities will also be
                                updated to use the custom breakpoints. Make sure to set grid values in <code>px</code>
                                (not <code>rem</code>, <code>em</code>, or <code>%</code>).</p>

                        </main>
                    </x-ui::card-body>
                </x-ui::card>

            </x-ui::col>
        </x-ui::row>
    </x-ui::container>
</x-app-layout>
