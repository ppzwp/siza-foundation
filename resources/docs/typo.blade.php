<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Getting Started') }}
        </h2>
    </x-slot>

    <x-slot name="sidebar">
        @include('docs::sidebar')
    </x-slot>

    <x-ui::container>
        <x-ui::row class="justify-content-center">
            <x-ui::col>
                <x-ui::card>
                    <x-ui::card-body class="px-5 shadow-sm">
                        <main role="main">
                            <h1>Typography</h1>
                            <p>
                                Documentation and examples for Bootstrap typography, including global settings,
                                headings, body text, lists, and more.
                            </p>

                            <h2><span class="bd-content-title">Global settings<a class="anchorjs-link "
                                                                                 aria-label="Anchor"
                                                                                 data-anchorjs-icon="#"
                                                                                 href="#global-settings"
                                                                                 style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Bootstrap sets basic global display, typography, and link styles. When more control is needed,
                                check out the <a href="/docs/4.6/utilities/text/">textual utility classes</a>.</p>
                            <ul>
                                <li>Use a <a href="/docs/4.6/content/reboot/#native-font-stack">native font stack</a> that
                                    selects the best <code>font-family</code> for each OS and device.
                                </li>
                                <li>For a more inclusive and accessible type scale, we use the browser’s default root <code>font-size</code>
                                    (typically 16px) so visitors can customize their browser defaults as needed.
                                </li>
                                <li>Use the <code>$font-family-base</code>, <code>$font-size-base</code>, and <code>$line-height-base</code>
                                    attributes as our typographic base applied to the <code>&lt;body&gt;</code>.
                                </li>
                                <li>Set the global link color via <code>$link-color</code> and apply link underlines only on
                                    <code>:hover</code>.
                                </li>
                                <li>Use <code>$body-bg</code> to set a <code>background-color</code> on the
                                    <code>&lt;body&gt;</code> (<code>#fff</code> by default).
                                </li>
                            </ul>
                            <p>These styles can be found within <code>_reboot.scss</code>, and the global variables are defined
                                in <code>_variables.scss</code>. Make sure to set <code>$font-size-base</code> in
                                <code>rem</code>.</p>
                            <h2 id="headings"><span class="bd-content-title">Headings<a class="anchorjs-link "
                                                                                        aria-label="Anchor"
                                                                                        data-anchorjs-icon="#" href="#headings"
                                                                                        style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>All HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code>, are available.</p>
                            <table>
                                <thead>
                                <tr>
                                    <th>Heading</th>
                                    <th>Example</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <code>&lt;h1&gt;&lt;/h1&gt;</code>
                                    </td>
                                    <td><span class="h1">h1. Bootstrap heading</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <code>&lt;h2&gt;&lt;/h2&gt;</code>
                                    </td>
                                    <td><span class="h2">h2. Bootstrap heading</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <code>&lt;h3&gt;&lt;/h3&gt;</code>
                                    </td>
                                    <td><span class="h3">h3. Bootstrap heading</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <code>&lt;h4&gt;&lt;/h4&gt;</code>
                                    </td>
                                    <td><span class="h4">h4. Bootstrap heading</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <code>&lt;h5&gt;&lt;/h5&gt;</code>
                                    </td>
                                    <td><span class="h5">h5. Bootstrap heading</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <code>&lt;h6&gt;&lt;/h6&gt;</code>
                                    </td>
                                    <td><span class="h6">h6. Bootstrap heading</span></td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">h1</span><span class="p">&gt;</span>h1. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">h1</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h2</span><span class="p">&gt;</span>h2. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">h2</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h3</span><span class="p">&gt;</span>h3. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">h3</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h4</span><span class="p">&gt;</span>h4. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">h4</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h5</span><span class="p">&gt;</span>h5. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">h5</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h6</span><span class="p">&gt;</span>h6. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">h6</span><span class="p">&gt;</span>
</code></pre>
                            </div>
                            <p><code>.h1</code> through <code>.h6</code> classes are also available, for when you want to match
                                the font styling of a heading but cannot use the associated HTML element.</p>
                            <div class="bd-example">
                                <p class="h1">h1. Bootstrap heading</p>
                                <p class="h2">h2. Bootstrap heading</p>
                                <p class="h3">h3. Bootstrap heading</p>
                                <p class="h4">h4. Bootstrap heading</p>
                                <p class="h5">h5. Bootstrap heading</p>
                                <p class="h6">h6. Bootstrap heading</p>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">p</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"h1"</span><span
                                                class="p">&gt;</span>h1. Bootstrap heading<span class="p">&lt;/</span><span
                                                class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"h2"</span><span class="p">&gt;</span>h2. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"h3"</span><span class="p">&gt;</span>h3. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"h4"</span><span class="p">&gt;</span>h4. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"h5"</span><span class="p">&gt;</span>h5. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"h6"</span><span class="p">&gt;</span>h6. Bootstrap heading<span
                                                class="p">&lt;/</span><span class="nt">p</span><span
                                                class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="customizing-headings"><span class="bd-content-title">Customizing headings<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#customizing-headings" style="padding-left: 0.375em;"></a></span></h3>
                            <p>Use the included utility classes to recreate the small secondary heading text from Bootstrap
                                3.</p>
                            <div class="bd-example">
                                <h3>
                                    Fancy display heading
                                    <small class="text-muted">With faded secondary text</small>
                                </h3>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">h3</span><span class="p">&gt;</span>
  Fancy display heading
  <span class="p">&lt;</span><span class="nt">small</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"text-muted"</span><span class="p">&gt;</span>With faded secondary text<span
                                                class="p">&lt;/</span><span class="nt">small</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">h3</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="display-headings"><span class="bd-content-title">Display headings<a class="anchorjs-link "
                                                                                                        aria-label="Anchor"
                                                                                                        data-anchorjs-icon="#"
                                                                                                        href="#display-headings"
                                                                                                        style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Traditional heading elements are designed to work best in the meat of your page content. When you
                                need a heading to stand out, consider using a <strong>display heading</strong>—a larger,
                                slightly more opinionated heading style. Keep in mind these headings are not responsive by
                                default, but it’s possible to enable <a href="#responsive-font-sizes">responsive font sizes</a>.
                            </p>
                            <div class="bd-example bd-example-type">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td><span class="display-1">Display 1</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="display-2">Display 2</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="display-3">Display 3</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="display-4">Display 4</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">h1</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"display-1"</span><span class="p">&gt;</span>Display 1<span class="p">&lt;/</span><span
                                                class="nt">h1</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h1</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"display-2"</span><span class="p">&gt;</span>Display 2<span class="p">&lt;/</span><span
                                                class="nt">h1</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h1</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"display-3"</span><span class="p">&gt;</span>Display 3<span class="p">&lt;/</span><span
                                                class="nt">h1</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">h1</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"display-4"</span><span class="p">&gt;</span>Display 4<span class="p">&lt;/</span><span
                                                class="nt">h1</span><span class="p">&gt;</span>
</code></pre>
                            </div>
                            <h2 id="lead"><span class="bd-content-title">Lead<a class="anchorjs-link " aria-label="Anchor"
                                                                                data-anchorjs-icon="#" href="#lead"
                                                                                style="padding-left: 0.375em;"></a></span></h2>
                            <p>Make a paragraph stand out by adding <code>.lead</code>.</p>
                            <div class="bd-example">
                                <p class="lead">
                                    This is a lead paragraph. It stands out from regular paragraphs.
                                </p>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">p</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"lead"</span><span class="p">&gt;</span>
  This is a lead paragraph. It stands out from regular paragraphs.
<span class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="inline-text-elements"><span class="bd-content-title">Inline text elements<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#inline-text-elements" style="padding-left: 0.375em;"></a></span></h2>
                            <p>Styling for common inline HTML5 elements.</p>
                            <div class="bd-example">
                                <p>You can use the mark tag to
                                    <mark>highlight</mark>
                                    text.
                                </p>
                                <p>
                                    <del>This line of text is meant to be treated as deleted text.</del>
                                </p>
                                <p><s>This line of text is meant to be treated as no longer accurate.</s></p>
                                <p>
                                    <ins>This line of text is meant to be treated as an addition to the document.</ins>
                                </p>
                                <p><u>This line of text will render as underlined</u></p>
                                <p><small>This line of text is meant to be treated as fine print.</small></p>
                                <p><strong>This line rendered as bold text.</strong></p>
                                <p><em>This line rendered as italicized text.</em></p>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;</span>You can use the mark tag to <span
                                                class="p">&lt;</span><span class="nt">mark</span><span class="p">&gt;</span>highlight<span
                                                class="p">&lt;/</span><span class="nt">mark</span><span class="p">&gt;</span> text.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">del</span><span
                                                class="p">&gt;</span>This line of text is meant to be treated as deleted text.<span
                                                class="p">&lt;/</span><span class="nt">del</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">s</span><span
                                                class="p">&gt;</span>This line of text is meant to be treated as no longer accurate.<span
                                                class="p">&lt;/</span><span class="nt">s</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">ins</span><span
                                                class="p">&gt;</span>This line of text is meant to be treated as an addition to the document.<span
                                                class="p">&lt;/</span><span class="nt">ins</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">u</span><span
                                                class="p">&gt;</span>This line of text will render as underlined<span class="p">&lt;/</span><span
                                                class="nt">u</span><span class="p">&gt;&lt;/</span><span
                                                class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">small</span><span
                                                class="p">&gt;</span>This line of text is meant to be treated as fine print.<span
                                                class="p">&lt;/</span><span class="nt">small</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">strong</span><span
                                                class="p">&gt;</span>This line rendered as bold text.<span
                                                class="p">&lt;/</span><span class="nt">strong</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">em</span><span
                                                class="p">&gt;</span>This line rendered as italicized text.<span
                                                class="p">&lt;/</span><span class="nt">em</span><span class="p">&gt;&lt;/</span><span
                                                class="nt">p</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <p><code>.mark</code> and <code>.small</code> classes are also available to apply the same styles as
                                <code>&lt;mark&gt;</code> and <code>&lt;small&gt;</code> while avoiding any unwanted semantic
                                implications that the tags would bring.</p>
                            <p>While not shown above, feel free to use <code>&lt;b&gt;</code> and <code>&lt;i&gt;</code> in
                                HTML5. <code>&lt;b&gt;</code> is meant to highlight words or phrases without conveying
                                additional importance while <code>&lt;i&gt;</code> is mostly for voice, technical terms, etc.
                            </p>
                            <h2 id="text-utilities"><span class="bd-content-title">Text utilities<a class="anchorjs-link "
                                                                                                    aria-label="Anchor"
                                                                                                    data-anchorjs-icon="#"
                                                                                                    href="#text-utilities"
                                                                                                    style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Change text alignment, transform, style, weight, and color with our <a
                                        href="/docs/4.6/utilities/text/">text utilities</a> and <a
                                        href="/docs/4.6/utilities/colors/">color utilities</a>.</p>
                            <h2 id="abbreviations"><span class="bd-content-title">Abbreviations<a class="anchorjs-link "
                                                                                                  aria-label="Anchor"
                                                                                                  data-anchorjs-icon="#"
                                                                                                  href="#abbreviations"
                                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>Stylized implementation of HTML’s <code>&lt;abbr&gt;</code> element for abbreviations and
                                acronyms to show the expanded version on hover. Abbreviations have a default underline and gain
                                a help cursor to provide additional context on hover and to users of assistive technologies.</p>
                            <p>Add <code>.initialism</code> to an abbreviation for a slightly smaller font-size.</p>
                            <div class="bd-example">
                                <p><abbr title="attribute">attr</abbr></p>
                                <p><abbr title="HyperText Markup Language" class="initialism">HTML</abbr></p>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">p</span><span
                                                class="p">&gt;&lt;</span><span class="nt">abbr</span> <span
                                                class="na">title</span><span class="o">=</span><span
                                                class="s">"attribute"</span><span class="p">&gt;</span>attr<span
                                                class="p">&lt;/</span><span class="nt">abbr</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;&lt;</span><span class="nt">abbr</span> <span
                                                class="na">title</span><span class="o">=</span><span class="s">"HyperText Markup Language"</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"initialism"</span><span class="p">&gt;</span>HTML<span class="p">&lt;/</span><span
                                                class="nt">abbr</span><span class="p">&gt;&lt;/</span><span
                                                class="nt">p</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="blockquotes"><span class="bd-content-title">Blockquotes<a class="anchorjs-link "
                                                                                              aria-label="Anchor"
                                                                                              data-anchorjs-icon="#"
                                                                                              href="#blockquotes"
                                                                                              style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <p>For quoting blocks of content from another source within your document. Wrap <code>&lt;blockquote
                                    class="blockquote"&gt;</code> around any <abbr title="HyperText Markup Language">HTML</abbr>
                                as the quote.</p>
                            <div class="bd-example">
                                <blockquote class="blockquote">
                                    <p class="mb-0">A well-known quote, contained in a blockquote element.</p>
                                </blockquote>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">blockquote</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"blockquote"</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"mb-0"</span><span class="p">&gt;</span>A well-known quote, contained in a blockquote element.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">blockquote</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="naming-a-source"><span class="bd-content-title">Naming a source<a class="anchorjs-link "
                                                                                                      aria-label="Anchor"
                                                                                                      data-anchorjs-icon="#"
                                                                                                      href="#naming-a-source"
                                                                                                      style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Add a <code>&lt;footer class="blockquote-footer"&gt;</code> for identifying the source. Wrap the
                                name of the source work in <code>&lt;cite&gt;</code>.</p>
                            <div class="bd-example">
                                <blockquote class="blockquote">
                                    <p class="mb-0">A well-known quote, contained in a blockquote element.</p>
                                    <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source
                                            Title</cite></footer>
                                </blockquote>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">blockquote</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"blockquote"</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"mb-0"</span><span class="p">&gt;</span>A well-known quote, contained in a blockquote element.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">footer</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"blockquote-footer"</span><span class="p">&gt;</span>Someone famous in <span
                                                class="p">&lt;</span><span class="nt">cite</span> <span
                                                class="na">title</span><span class="o">=</span><span
                                                class="s">"Source Title"</span><span class="p">&gt;</span>Source Title<span
                                                class="p">&lt;/</span><span class="nt">cite</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">footer</span><span
                                                class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">blockquote</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="alignment"><span class="bd-content-title">Alignment<a class="anchorjs-link "
                                                                                          aria-label="Anchor"
                                                                                          data-anchorjs-icon="#"
                                                                                          href="#alignment"
                                                                                          style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Use text utilities as needed to change the alignment of your blockquote.</p>
                            <div class="bd-example">
                                <blockquote class="blockquote text-center">
                                    <p class="mb-0">&gt;A well-known quote, contained in a blockquote element.</p>
                                    <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source
                                            Title</cite></footer>
                                </blockquote>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">blockquote</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"blockquote text-center"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"mb-0"</span><span class="p">&gt;</span>&gt;A well-known quote, contained in a blockquote element.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">footer</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"blockquote-footer"</span><span class="p">&gt;</span>Someone famous in <span
                                                class="p">&lt;</span><span class="nt">cite</span> <span
                                                class="na">title</span><span class="o">=</span><span
                                                class="s">"Source Title"</span><span class="p">&gt;</span>Source Title<span
                                                class="p">&lt;/</span><span class="nt">cite</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">footer</span><span
                                                class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">blockquote</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <div class="bd-example">
                                <blockquote class="blockquote text-right">
                                    <p class="mb-0">A well-known quote, contained in a blockquote element.</p>
                                    <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source
                                            Title</cite></footer>
                                </blockquote>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">blockquote</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"blockquote text-right"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">p</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"mb-0"</span><span class="p">&gt;</span>A well-known quote, contained in a blockquote element.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">footer</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"blockquote-footer"</span><span class="p">&gt;</span>Someone famous in <span
                                                class="p">&lt;</span><span class="nt">cite</span> <span
                                                class="na">title</span><span class="o">=</span><span
                                                class="s">"Source Title"</span><span class="p">&gt;</span>Source Title<span
                                                class="p">&lt;/</span><span class="nt">cite</span><span
                                                class="p">&gt;&lt;/</span><span class="nt">footer</span><span
                                                class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">blockquote</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="lists"><span class="bd-content-title">Lists<a class="anchorjs-link " aria-label="Anchor"
                                                                                  data-anchorjs-icon="#" href="#lists"
                                                                                  style="padding-left: 0.375em;"></a></span>
                            </h2>
                            <h3 id="unstyled"><span class="bd-content-title">Unstyled<a class="anchorjs-link "
                                                                                        aria-label="Anchor"
                                                                                        data-anchorjs-icon="#" href="#unstyled"
                                                                                        style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Remove the default <code>list-style</code> and left margin on list items (immediate children
                                only). <strong>This only applies to immediate children list items</strong>, meaning you will
                                need to add the class for any nested lists as well.</p>
                            <div class="bd-example">
                                <ul class="list-unstyled">
                                    <li>This is a list.</li>
                                    <li>It appears completely unstyled.</li>
                                    <li>Structurally, it's still a list.</li>
                                    <li>However, this style only applies to immediate child elements.</li>
                                    <li>Nested lists:
                                        <ul>
                                            <li>are unaffected by this style</li>
                                            <li>will still show a bullet</li>
                                            <li>and have appropriate left margin</li>
                                        </ul>
                                    </li>
                                    <li>This may still come in handy in some situations.</li>
                                </ul>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">ul</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"list-unstyled"</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>This is a list.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>It appears completely unstyled.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>Structurally, it's still a list.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>However, this style only applies to immediate child elements.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>Nested lists:
    <span class="p">&lt;</span><span class="nt">ul</span><span class="p">&gt;</span>
      <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>are unaffected by this style<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
      <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>will still show a bullet<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
      <span class="p">&lt;</span><span class="nt">li</span><span
                                                class="p">&gt;</span>and have appropriate left margin<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
    <span class="p">&lt;/</span><span class="nt">ul</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span><span class="p">&gt;</span>This may still come in handy in some situations.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">ul</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="inline"><span class="bd-content-title">Inline<a class="anchorjs-link " aria-label="Anchor"
                                                                                    data-anchorjs-icon="#" href="#inline"
                                                                                    style="padding-left: 0.375em;"></a></span>
                            </h3>
                            <p>Remove a list’s bullets and apply some light <code>margin</code> with a combination of two
                                classes, <code>.list-inline</code> and <code>.list-inline-item</code>.</p>
                            <div class="bd-example">
                                <ul class="list-inline">
                                    <li class="list-inline-item">This is a list item.</li>
                                    <li class="list-inline-item">And another one.</li>
                                    <li class="list-inline-item">But they're displayed inline.</li>
                                </ul>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">ul</span> <span
                                                class="na">class</span><span class="o">=</span><span
                                                class="s">"list-inline"</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"list-inline-item"</span><span class="p">&gt;</span>This is a list item.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"list-inline-item"</span><span
                                                class="p">&gt;</span>And another one.<span class="p">&lt;/</span><span
                                                class="nt">li</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">li</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"list-inline-item"</span><span class="p">&gt;</span>But they're displayed inline.<span
                                                class="p">&lt;/</span><span class="nt">li</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">ul</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h3 id="description-list-alignment"><span class="bd-content-title">Description list alignment<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#description-list-alignment" style="padding-left: 0.375em;"></a></span></h3>
                            <p>Align terms and descriptions horizontally by using our grid system’s predefined classes (or
                                semantic mixins). For longer terms, you can optionally add a <code>.text-truncate</code> class
                                to truncate the text with an ellipsis.</p>
                            <div class="bd-example">
                                <dl class="row">
                                    <dt class="col-sm-3">Description lists</dt>
                                    <dd class="col-sm-9">A description list is perfect for defining terms.</dd>

                                    <dt class="col-sm-3">Term</dt>
                                    <dd class="col-sm-9">
                                        <p>Definition for the term.</p>
                                        <p>And some more placeholder definition text.</p>
                                    </dd>

                                    <dt class="col-sm-3">Another term</dt>
                                    <dd class="col-sm-9">This definition is short, so no extra paragraphs or anything.</dd>

                                    <dt class="col-sm-3 text-truncate">Truncated term is truncated</dt>
                                    <dd class="col-sm-9">This can be useful when space is tight. Adds an ellipsis at the end.
                                    </dd>

                                    <dt class="col-sm-3">Nesting</dt>
                                    <dd class="col-sm-9">
                                        <dl class="row">
                                            <dt class="col-sm-4">Nested definition list</dt>
                                            <dd class="col-sm-8">I heard you like definition lists. Let me put a definition list
                                                inside your definition list.
                                            </dd>
                                        </dl>
                                    </dd>
                                </dl>
                            </div>

                            <div class="highlight"><pre class="chroma"><code class="language-html" data-lang="html"><span
                                                class="p">&lt;</span><span class="nt">dl</span> <span
                                                class="na">class</span><span class="o">=</span><span class="s">"row"</span><span
                                                class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-3"</span><span class="p">&gt;</span>Description lists<span
                                                class="p">&lt;/</span><span class="nt">dt</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>A description list is perfect for defining terms.<span
                                                class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>

  <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-3"</span><span class="p">&gt;</span>Term<span
                                                class="p">&lt;/</span><span class="nt">dt</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;</span>Definition for the term.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">p</span><span class="p">&gt;</span>And some more placeholder definition text.<span
                                                class="p">&lt;/</span><span class="nt">p</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>

  <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-3"</span><span class="p">&gt;</span>Another term<span
                                                class="p">&lt;/</span><span class="nt">dt</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>This definition is short, so no extra paragraphs or anything.<span
                                                class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>

  <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-3 text-truncate"</span><span class="p">&gt;</span>Truncated term is truncated<span
                                                class="p">&lt;/</span><span class="nt">dt</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>This can be useful when space is tight. Adds an ellipsis at the end.<span
                                                class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>

  <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-3"</span><span class="p">&gt;</span>Nesting<span class="p">&lt;/</span><span
                                                class="nt">dt</span><span class="p">&gt;</span>
  <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-9"</span><span class="p">&gt;</span>
    <span class="p">&lt;</span><span class="nt">dl</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"row"</span><span class="p">&gt;</span>
      <span class="p">&lt;</span><span class="nt">dt</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-4"</span><span
                                                class="p">&gt;</span>Nested definition list<span class="p">&lt;/</span><span
                                                class="nt">dt</span><span class="p">&gt;</span>
      <span class="p">&lt;</span><span class="nt">dd</span> <span class="na">class</span><span class="o">=</span><span
                                                class="s">"col-sm-8"</span><span class="p">&gt;</span>I heard you like definition lists. Let me put a definition list inside your definition list.<span
                                                class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>
    <span class="p">&lt;/</span><span class="nt">dl</span><span class="p">&gt;</span>
  <span class="p">&lt;/</span><span class="nt">dd</span><span class="p">&gt;</span>
<span class="p">&lt;/</span><span class="nt">dl</span><span class="p">&gt;</span></code></pre>
                            </div>
                            <h2 id="responsive-font-sizes"><span class="bd-content-title">Responsive font sizes<a
                                            class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
                                            href="#responsive-font-sizes" style="padding-left: 0.375em;"></a></span></h2>
                            <p>As of v4.3.0, Bootstrap ships with the option to enable responsive font sizes, allowing text to
                                scale more naturally across device and viewport sizes. <abbr
                                        title="Responsive font sizes">RFS</abbr> can be enabled by changing the <code>$enable-responsive-font-sizes</code>
                                Sass variable to <code>true</code> and recompiling Bootstrap.</p>
                            <p>To support <abbr title="Responsive font sizes">RFS</abbr>, we use a Sass mixin to replace our
                                normal <code>font-size</code> properties. Responsive font sizes will be compiled into <code>calc()</code>
                                functions with a mix of <code>rem</code> and viewport units to enable the responsive scaling
                                behavior. More about <abbr title="Responsive font sizes">RFS</abbr> and its configuration can be
                                found on its <a href="https://github.com/twbs/rfs/tree/v8.0.4">GitHub repository</a>.</p>

                        </main>
                    </x-ui::card-body>
                </x-ui::card>

            </x-ui::col>
        </x-ui::row>
    </x-ui::container>
</x-app-layout>
