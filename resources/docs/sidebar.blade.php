<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="{{ url('/siza/docs') }}">
        <i class="fa fa-home c-sidebar-nav-icon"></i>
        Getting Started
    </a>

    <a class="c-sidebar-nav-link" href="{{ url('/siza/docs/grid') }}">
        <i class="fa fa-home c-sidebar-nav-icon"></i>
        Grid
    </a>

    <a class="c-sidebar-nav-link" href="{{ url('/siza/docs/typo') }}">
        <i class="fa fa-home c-sidebar-nav-icon"></i>
        Typography
    </a>

    <a class="c-sidebar-nav-link" href="{{ url('/siza/docs/tables') }}">
        <i class="fa fa-home c-sidebar-nav-icon"></i>
        Tables
    </a>
</li>