@props([
    'class' => 'form-control',
])

<input
    {!! $attributes->merge(['class' => $class]) !!}
>