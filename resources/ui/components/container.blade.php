@props([
    'class' => ''
])

@php
    if (empty($class)) {
        $class = 'container';
    }
@endphp

<div {{ $attributes->merge(['class' => $class]) }}>
    {{ $slot }}
</div>