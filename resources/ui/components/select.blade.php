<select {{ $attributes->merge(['class' => 'custom-select']) }}>
    {{ $slot }}
</select>