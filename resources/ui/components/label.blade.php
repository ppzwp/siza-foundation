<label {!! $attributes->merge(['class' => 'col-form-label']) !!}>
    {{ $slot }}
</label><!-- /.label -->