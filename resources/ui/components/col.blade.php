@props([
    'class' => 'col'
])
<div {{ $attributes->merge(['class' => $class]) }}>
    {{ $slot }}
</div>