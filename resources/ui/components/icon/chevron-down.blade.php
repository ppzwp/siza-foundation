<svg class="{{ $class ?? 'ml-2' }}" width="{{ $width ?? '16' }}" fill="none" viewBox="0 0 24 24" stroke="currentColor">
    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
</svg>