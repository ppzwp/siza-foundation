@props([
    'type' => 'primary',
])

@php
$class = 'alert ';
switch ($type) {
    default:
    case 'primary':
        $class .= 'alert-primary';
        break;

    case 'danger':
        $class .= 'alert-danger';
        break;

    case 'warning':
        $class .= 'alert-warning';
        break;

    case 'info':
        $class .= 'alert-info';
        break;

    case 'success':
        $class .= 'alert-success';
        break;

    case 'secondary':
        $class .= 'alert-secondary';
        break;
}
@endphp

<div {{ $attributes->merge(['class' => $class]) }}>
    {{ $slot }}
</div>