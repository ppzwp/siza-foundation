@props([
    'action',
    'method' => 'post'
])

<x-ui::row {{ $attributes->merge(['class' => 'row']) }}>
    <x-ui::col class="col-md-4">
        <x-ui::section-title>
            <x-slot name="title">{{ $title }}</x-slot>
            <x-slot name="description">
                <span class="small">
                    {{ $description }}
                </span>
            </x-slot>
        </x-ui::section-title>
    </x-ui::col>
    <x-ui::col class="col-md-8">
        <x-ui::card class="shadow-sm">
            <form action="{{ $action }}" method="{{ ($method == 'get') ? 'get' : 'post' }}">
                @csrf
                @if ($method != 'post')
                    @method($method)
                @endif
                <x-ui::card-body>
                    {{ $slot }}
                </x-ui::card-body>

                @if (isset($actions))
                    <x-ui::card-footer class="d-flex justify-content-end">
                        {{ $actions }}
                    </x-ui::card-footer>
                @endif
            </form>
        </x-ui::card>
    </x-ui::col>
</x-ui::row>
