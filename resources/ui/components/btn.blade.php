@props([
    'class' => 'btn btn-primary',
    'href' => null,
])

@if (is_null($class))
    @php
      $class = 'btn btn-primary';
    @endphp
@endif

@if (is_null($href))
    <button {{ $attributes->merge(['type' => 'submit', 'class' => 'btn '.$class]) }}>
        {{ $slot }}
    </button>
@else
    <a {{ $attributes->merge(['class' => 'btn '.$class, 'href' => $href]) }}>
        {{ $slot }}
    </a>
@endif

