@props([
    'type' => 'primary',
])

@php
$class = 'badge ';
switch ($type) {
    default:
    case 'primary':
        $class .= 'badge-primary';
        break;

    case 'danger':
        $class .= 'badge-danger';
        break;

    case 'warning':
        $class .= 'badge-warning';
        break;

    case 'info':
        $class .= 'badge-info';
        break;

    case 'success':
        $class .= 'badge-success';
        break;

    case 'secondary':
        $class .= 'badge-secondary';
        break;
}
@endphp

<div {{ $attributes->merge(['class' => $class]) }}>
    {{ $slot }}
</div>