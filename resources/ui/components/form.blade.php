@props(['method' => ''])
@php
    $type = 'get';

    switch (strtolower($method)) {
        case 'put':
        case 'delete':
        case 'post':
            $type = 'post';
            break;
    }
@endphp
<form {!! $attributes->merge(['class' => '']) !!} method="{{ $type }}">
    @csrf
    @switch (strtolower($method))
        @case('put')
            @method('put')
            @break
        @case('delete')
            @method('delete')
            @break
    @endswitch

    {{ $slot }}
</form>