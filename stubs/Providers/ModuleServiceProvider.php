<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // get all modules folder
        $modules = File::directories(app_path('Modules'));

        if (count($modules)) {
            foreach ($modules as $module) {

                // load modules service provider
                $config = require_once $module.'/config.php';

                // Load ServiceProvider
                App::register($config['provider']);
            }
        }
    }
}
