@extends('siza::layouts.dashboard')

@section('page-heading', 'Blank Page')
@section('page-subheading', 'Get Started')

@section('breadcrumb')
    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-alt">
            <li class="breadcrumb-item">
                <a class="link-fx" href="">Dashboard</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
                Blank
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="card">
            <div class="card-header">
                <h3 class="block-title">Block Title</h3>
            </div>
            <div class="card-body">
                <p class="font-size-sm text-muted">
                    Your content..
                </p>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
