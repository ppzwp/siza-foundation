<?php

return [
    'name' => '[[MODULE_NAME]]',
    'description' => null, // Just pust it here, maybe we need it in the future, yeah, maybe ...
    'icon' => null, // Just pust it here, maybe we need it in the future, yeah, maybe ...
    'menu_label' => '[[MODULE_NAME]]',
    'url' => '[[DEFAULT_ROUTE]]',
    'provider' => \App\Modules\[[FOLDER_NAME]]\ServiceProvider::class,
    'permission' => null,
    'show_in_dashboard' => true,
];
