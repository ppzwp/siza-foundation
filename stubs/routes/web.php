<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Siza\Foundation\App\Http\Controllers\LoginController;

Route::get('/', function () {
    return redirect()->to('/login');
})->middleware(['web']);

Route::post('/callback/login', [LoginController::class, 'callback'])
    ->middleware('web');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', [DashboardController::class, 'dashboard'])
        ->name('dashboard');

    // This is where your routes goes

});