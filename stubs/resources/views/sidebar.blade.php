@foreach (getAllModules(true) as $module)
    @permission($module['permission'])
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{ route($module['url']) }}">
            <i class="{{ $module['icon'] ?? 'fa fa-puzzle-piece' }} c-sidebar-nav-icon"></i>
            {{ $module['name'] }}
        </a>
    </li>
    @endpermission
@endforeach
