<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Menu Utama') }}
        </h2>
    </x-slot>

    <x-slot name="sidebar">
        @include('sidebar')
    </x-slot>

    <x-ui::card class="shadow-sm">
        <x-ui::card-header>Hello</x-ui::card-header>
        <x-ui::card-body>
            Hello World
        </x-ui::card-body>
    </x-ui::card>

    <x-ui::alert type="secondary">
        This is alert primary
    </x-ui::alert>
</x-app-layout>
