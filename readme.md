# SIZA Core

SIZA Core dibangunkan bertujuan untuk menawarkan fungsi-fungsi asas yang membantu memudahkan programmer menjalankan tugasa dengan lebih efisyen.

Antara ciri yang ditawarkan ialah:
- Theme standard (menggunakan OneUI)
- Single sign-on (tidak menggunakan Laravel Passport)
- Model untuk table di SIZA

## Pemasangan

Tambah di fail `composer.json`

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://git.zakat.my/packages/siza-core.git"
    }
],
```

Jalankan arahan di Terminal

```shell script
composer require siza/core

php artisan vendor:publish --tag=siza-foundation-asset
php artisan vendor:publish --tag=siza-foundation-config
php artisan vendor:publish --tag=siza-foundation-view
```

## Database Connection

Ubah sambungan database di fail `.env` 

```
DB_CONNECTION=siza
DB_HOST=10.10.1.8
DB_PORT=1521
DB_DATABASE=siza
DB_USERNAME=ppz
DB_PASSWORD=ppzdev
```

## User Model

Tukar isi kandungan `/app/User.php` dengan kod di bawah:

```php
<?php 

namespace App;

use Siza\Foundation\App\Models\User as SIZAUser;

class User extends SIZAUser {

}
``` 

## Layout

Gunakan layout yang disediakan pada view

```
@extends('siza-core::layouts.dashboard')
```

## Blank View

```
@extends('siza-core::layouts.dashboard')

@section('page-heading', 'Blank Page')
@section('page-subheading', 'Get Started')

@section('breadcrumbs')
    <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-alt">
            <li class="breadcrumb-item">
                <a class="link-fx" href="">Dashboard</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
                Blank
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="content">
        <!-- Your Block -->
        <div class="card">
            <div class="card-header">
                <h3 class="block-title">Block Title</h3>
            </div>
            <div class="card-body">
                <p class="font-size-sm text-muted">
                    Your content..
                </p>
            </div>
        </div>
        <!-- END Your Block -->
    </div>
    <!-- END Page Content -->
@endsection
```


