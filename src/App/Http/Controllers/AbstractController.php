<?php

namespace Siza\Foundation\App\Http\Controllers;

use Illuminate\Routing\Controller;

abstract class AbstractController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }
}
