<?php

namespace Siza\Foundation\App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends AbstractController
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * SSO login redirect
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function dashboard(Request $request)
    {
        return view('siza::dashboard.index');
    }
}