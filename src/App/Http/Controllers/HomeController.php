<?php

namespace Siza\Foundation\App\Http\Controllers;

class HomeController extends AbstractController
{
    public function home()
    {
        return view('siza::home');
    }
}
