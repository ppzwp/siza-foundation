<?php

namespace Siza\Foundation\App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Siza\Foundation\App\Http\Helpers\Str;

class LoginController extends AbstractController
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * SSO login redirect
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        // Get current app domain
        $callback = $request->root();

        if (! session()->has('key')) {
            session()->put('key', session()->getId());
        }

        $key = session()->get('key');

        // Redirect to main app for authentication process
        return redirect()->to(config('siza-foundation.url').'/dashboard?callback='.$callback.'&key='.$key);
    }

    /**
     * Login the user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(Request $request)
    {
        // Force logout previous session
        Auth::guard('web')->logout();

        Auth::guard('web')->loginUsingId($request->id);

        return redirect()->to('dashboard');
    }
}
