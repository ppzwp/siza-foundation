<?php

namespace Siza\Foundation\App\Http\Controllers;

use Illuminate\Http\Request;
use Siza\Foundation\App\Http\Responses\LogoutResponse;

class LogoutController extends AbstractController
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * SSO login redirect
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $this->guard->logout();
        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return app(LogoutResponse::class);
    }
}