<?php

namespace Siza\Foundation\App\Http\Responses;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Fortify\Contracts\LogoutResponse as LogoutResponseContract;
use Symfony\Component\HttpFoundation\Response;

class LogoutResponse implements LogoutResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function toResponse($request)
    {
        $redirectUrl = redirect('/');

        if (config('siza-foundation.sub_app') AND config('siza-foundation.use_remote_auth')) {
            $redirectUrl = redirect(config('siza-foundation.url').'/dashboard');
        }

        return $request->wantsJson()
            ? new JsonResponse('', 204)
            : $redirectUrl;
    }
}
