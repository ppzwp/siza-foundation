<?php

namespace Siza\Foundation\App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ModulePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $id)
    {
        if (auth()->user()->hasAccessLevel($id)) {
            return $next($request);
        }

        abort(403, 'Anda tiada hak akses!');
    }
}
