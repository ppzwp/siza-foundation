<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DebugHelper
{
    public static function log($empId, $request, $referer)
    {
        try {
            DB::connection('debug')
                ->table('login')
                ->insert([
                    'emp_id' => $empId,
                    'request' => json_encode($request),
                    'referer' => $referer,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}