<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Siza\Database\Models\Zakat2u\ZoUser;

class PaymentRecord
{
    /**
     * Get user payment records
     *
     * @param $id
     * @return mixed
     */
    public static function isPaidUser($id)
    {
        $user = ZoUser::find($id);

        if ($user) {
            return Cache::remember('user-kutipan-' . md5($id), config('siza-foundation.query_cache'), function () use ($user) {
                $years = self::getYearStartPaid($user);

                return $years === false ? 0 : count($years);
            });
        }

        return false;
    }

    /**
     * Get user payment years
     *
     * @param $user
     * @return bool
     */
    public static function yearPaid($user)
    {
        $kutipan = DB::table('kutipan')
            ->selectRaw("DISTINCT to_char(kutipan.TKH_KUTIPAN, 'yyyy') as tahun")
            ->leftJoin('resit', 'resit.kut_kut_id', '=', 'kutipan.kut_id')
            ->where(function ($query) {
                $query
                    ->whereRaw("resit.status = 'Y'")
                    ->whereRaw("kutipan.jenis_trans != 'BTL'");
            })
            ->where('kutipan.pby_no_k_p_lama', $user->getOldIc())
            ->orWhere('kutipan.pby_no_k_p_lama', $user->getUsername())
            ->orderBy('tahun', 'asc')
            ->get();

        if (!is_null($kutipan)) {
            return $kutipan;
        }

        return false;
    }

    /**
     * Get user payment records
     *
     * @param $user
     * @param int $tahun
     * @return array
     */
    public static function getPaymentRecords($user, $tahun = 0)
    {
        $tahun = (int) $tahun;

        if ($tahun == 0 || !is_numeric($tahun) || $tahun < 1997) {

            $results = DB::select(DB::raw("select kjeniszkt_ubah.kut_kut_id, kutipan.tkh_kutipan as tarikh,
                    zakat.nama_penuh, to_char(kjeniszkt_ubah.amaun,'9999999.99') as jumlah, kjeniszkt_ubah.thn_haul_m,
                    nvl(concat(resit.kodresit, resit.noresit),kutipan.old_recptno) as no_resit,
                    to_char( sum(sum(kjeniszkt_ubah.amaun)) over (order by kutipan.tkh_kutipan rows between unbounded preceding and current row),'9999999.99') as cumm,
                    kpgaji.bulan_potongan as bln_potongan, kpgaji.tahun_potongan as tahun_potongan 
                from kutipan, kpgaji,kjeniszkt_ubah,zakat,resit,pbayar
                where kpgaji.kut_kut_id=kut_id 
                and resit.kut_kut_id=kut_id 
                and resit.status = 'Y'
                and (pbayar.no_k_p_lama = '{$user->getOldIc()}' or pbayar.no_k_p_lama = '{$user->getUsername()}')
                and kutipan.pby_no_k_p_lama = pbayar.no_k_p_lama
                and kjeniszkt_ubah.kut_kut_id=kut_id
                and zkt_kod_zakat = zakat.kod_zakat
                group by kjeniszkt_ubah.kut_kut_id, kutipan.tkh_kutipan, zakat.nama_penuh, kjeniszkt_ubah.amaun,
                    kjeniszkt_ubah.thn_haul_m, resit.kodresit, resit.noresit, kutipan.old_recptno, kpgaji.bulan_potongan, kpgaji.tahun_potongan
                order by to_number(kpgaji.bulan_potongan)"));
        }
        else {
            $results = DB::select(DB::raw("select kjeniszkt_ubah.kut_kut_id, kutipan.tkh_kutipan as tarikh,
                    zakat.nama_penuh, to_char(kjeniszkt_ubah.amaun,'9999999.99') as jumlah, kjeniszkt_ubah.thn_haul_m,
                    nvl(concat(resit.kodresit, resit.noresit),kutipan.old_recptno) as no_resit,
                    to_char( sum(sum(kjeniszkt_ubah.amaun)) over (order by kutipan.tkh_kutipan rows between unbounded preceding and current row),'9999999.99') as cumm,
                    kpgaji.bulan_potongan as bln_potongan, kpgaji.tahun_potongan as tahun_potongan 
                from kutipan, kpgaji,kjeniszkt_ubah,zakat,resit,pbayar
                where kpgaji.kut_kut_id=kut_id 
                and resit.kut_kut_id=kut_id 
                and resit.status = 'Y'
                and tahun_potongan = '{$tahun}'
                and (pbayar.no_k_p_lama = '{$user->getOldIc()}' or pbayar.no_k_p_lama = '{$user->getUsername()}')
                and kutipan.pby_no_k_p_lama = pbayar.no_k_p_lama
                and kjeniszkt_ubah.kut_kut_id=kut_id
                and zkt_kod_zakat = zakat.kod_zakat
                group by kjeniszkt_ubah.kut_kut_id, kutipan.tkh_kutipan, zakat.nama_penuh, kjeniszkt_ubah.amaun,
                    kjeniszkt_ubah.thn_haul_m, resit.kodresit, resit.noresit, kutipan.old_recptno, kpgaji.bulan_potongan, kpgaji.tahun_potongan
                order by to_number(kpgaji.bulan_potongan)"));
        }

        return $results;
    }
}
