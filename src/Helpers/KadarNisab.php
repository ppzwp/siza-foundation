<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\DB;
use Siza\Database\Models\Spz\VwKadarNisab;

class KadarNisab
{
    /**
     * Get all nisab
     *
     * @return mixed
     */
    public static function get()
    {
        return VwKadarNisab::select(DB::raw('tahun as year'), DB::raw('nisab_tahunan as amount'))
            ->orderBy('tahun', 'desc')
            ->get();
    }

    /**
     * Get latest nisab
     *
     * @return mixed
     */
    public static function latest()
    {
        return VwKadarNisab::select(DB::raw('tahun as year'), DB::raw('nisab_tahunan as amount'))
            ->orderBy('tahun', 'desc')
            ->first();
    }
}
