<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\Cache;
use Siza\Database\Models\Kutipan;
use Siza\Database\Models\Pbayar;

class PembayarHelper
{
    /**
     * Get pbayar record
     * 
     * @param $kp
     * @return mixed
     */
    public static function get($kp)
    {
        return Cache::remember('pbayar-'.$kp, 600, function () use ($kp) {
            return Pbayar::where('no_k_p_lama', $kp)
                ->orWhere('no_k_p_baru', $kp)
                ->orWhere('no_k_p_lama', strtoupper($kp))
                ->orWhere('no_k_p_lama', strtolower($kp))
                ->orWhere('no_k_p_baru', strtoupper($kp))
                ->orWhere('no_k_p_baru', strtolower($kp))
                ->first();
        });
    }

    /**
     * Bayaran terkini pembayar
     *
     * @param $kp
     * @return mixed
     */
    public static function bayaranTerkini($kp)
    {
        $pbayar = self::get($kp);

        if (! $pbayar) {
            return false;
        }

        return Cache::remember('pbayar-'.$kp.'-bayaran-terkini', 600, function () use ($pbayar) {
            return Kutipan::where('pby_no_k_p_lama', $pbayar->no_k_p_lama)
                ->orderBy('kut_id', 'desc')
                ->first();
        });
    }
}