<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Siza\Database\App\Models\Spsm\Employee;
use Siza\Database\App\Models\Spsm\LoginWeb;

class SizaLoginHelper
{
    public static function login($id, $password)
    {
        try {
            $authenticate = DB::select(DB::raw("
                select count(*) as result from pengguna
                where enpwd(password, 'GET') = '{$password}' 
                and (
                    emp_id = '{$id}' or nama_login = '{$id}' 
                "));

            return (int) $authenticate[0]->result > 0;
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return false;
    }

    public static function setData($data = [])
    {
        Session::put('employee', $data);
    }
}