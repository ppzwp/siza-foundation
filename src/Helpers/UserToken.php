<?php

namespace Siza\Foundation\Helpers;

class UserToken
{
    /**
     * Generate user auth_key and auth_secret
     *
     * @param $id   int     User ID from database
     * @return array
     */
    public static function generate(int $id): array
    {
        $key = self::key($id);

        return [
            'key' => $key,
            'secret' => password_hash(self::secret($id, $key), PASSWORD_BCRYPT)
        ];
    }

    /**
     * Verify hash value
     *
     * @param $id   int         User ID from database
     * @param $key  string      User auth_key
     * @param $secret string    User auth_secret
     * @return bool
     */
    public static function verify(int $id, string $key, string $secret): bool
    {
        return password_verify(self::secret($id, $key), $secret);
    }

    /**
     * Generate key
     *
     * @param $id   int         User ID from database
     * @return string
     */
    public static function key(int $id): string
    {
        return sha1(microtime() . $id);
    }

    /**
     * Generate key combination
     *
     * @param $id   int     User ID from database
     * @param $key  string  User generated key
     * @return string
     */
    public static function secret(int $id, string $key): string
    {
        return $id . ':' . $key;
    }
}
