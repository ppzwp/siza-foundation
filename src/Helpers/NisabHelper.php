<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\Cache;
use Siza\Database\Models\Spz\SpzNisabTahunan;

class NisabHelper
{
    /**
     * Get nisab value for single year
     *
     * @param int|null $year
     * @return float
     */
    public static function get(int $year = null)
    {
        $year = is_null($year) ? date('Y') : $year;
        
        return Cache::rememberForever('nisab-tahun-'.$year, function () use ($year) {
            $nisab = SpzNisabTahunan::where('tahun', $year)->first();

            if (! $nisab) {
                return self::get($year - 1);
            }

            return (float) $nisab->nisab;
        });
    }
}