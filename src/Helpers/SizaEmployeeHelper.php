<?php

namespace Siza\Foundation\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Siza\Database\App\Models\Spsm\Employee;

class SizaEmployeeHelper
{
    public static function get($id)
    {
        $employee = Employee::where('emp_id', $id)
            ->first();

        if ($employee) {
            return [
                'emp_id' => $id,
                'title' => $employee->emp_title,
                'nama' => $employee->emp_name,
                'no_kp_baru' => @$employee->detail->no_k_p_baru,
                'no_kp_lama' => @$employee->detail->no_k_p_lama,
                'telefon' => @$employee->detail->no_tel2,
                'emel' => @$employee->detail->email,
                'alamat' => @$employee->detail->alamat,
                'bandar' => @$employee->detail->bandar,
                'bandar2' => @$employee->detail->bandar2,
                'poskod' => @$employee->detail->poskod,
                'negeri' => @$employee->detail->negeri,
                'jawatan' => @$employee->jawatan->butiran,
                'bahagian' => @$employee->bahagian->butiran,
                'unit' => @$employee->unit->butiran,
                'gred' => @$employee->gred->butiran,
            ];
        }

        return false;
    }
}