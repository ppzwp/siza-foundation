<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;

/**
 * Get staff information
 *
 * @param null $empId
 * @return array|false
 */
function employee($empId = null)
{
    $empId = ! is_null($empId) ? $empId : auth()->user()->emp_id;

    return \Siza\Foundation\Helpers\SizaEmployeeHelper::get($empId);
}

/**
 * Return staff profile picture
 *
 * @param null $empId
 * @return string
 */
function avatar($empId = null)
{
    $empId = ! is_null($empId) ? $empId : auth()->user()->emp_id;

    return rtrim(config('siza-foundation.url'), '/').'/storage/avatar/'.$empId.'.png';
}

/**
 * Load user CSS file
 *
 * @param null $empId
 * @return null|string
 */
function userCss($empId = null)
{
    $color = setting('color_scheme');

    if (! is_null($color)) {
        $prefix = '';

        if (config('siza-foundation.sub_app')) {
            $prefix = rtrim(config('siza-foundation.url'), '/');
        }

        $url =  $prefix . '/css/color-scheme/' . $color . '.css';

        return '<link rel="stylesheet" href="' . $url . '">';
    }

    return null;
}

/**
 * Get user setting by key
 *
 * @param $key string|array
 * @param null $userId
 * @return false|mixed
 */
function setting($key, $userId = null)
{
    $userId = !is_null($userId) ? $userId : auth()->user()->emp_id;

    if (is_array($key)) {
        foreach ($key as $k => $v) {
            // find existing key
            $setting = \Illuminate\Support\Facades\DB::connection('sso')
                ->table('employee_settings')
                ->where('key', $key)
                ->where('emp_id', $userId)
                ->first();

            // key exists
            if ($setting) {
                $setting->update([
                    $k => $v
                ]);
            }
            // not exists, insert new
            else {
                \Illuminate\Support\Facades\DB::connection('sso')
                    ->table('employee_settings')
                    ->insert([
                        $k => $v,
                        'emp_id' => $userId
                    ]);
            }
        }
    }
    else {
        return \Illuminate\Support\Facades\Cache::rememberForever('setting-' . $key . '-' . $userId, function () use ($userId, $key) {
            $setting = \Illuminate\Support\Facades\DB::connection('sso')
                ->table('employee_settings')
                ->select('value')
                ->where('key', $key)
                ->where('emp_id', $userId)
                ->first();

            if (!$setting) {
                return null;
            }

            return $setting->value;
        });
    }
}

/**
 * Get all app modules
 * 
 * @return array
 */
function appModules()
{
    $modules = \Illuminate\Support\Facades\Cache::remember('app-modules', 60, function () {
        return \Illuminate\Support\Facades\DB::connection('sso')
            ->table('modules')
            ->where('core', false)
            ->where('active', true)
            ->orderBy('ordering')
            ->orderBy('name')
            ->get();
    });

    $array = [];

    if ($modules) {
        foreach ($modules as $module) {
            $array[] = [
                'name' => $module->name,
                'url' => $module->url,
            ];
        }
    }

    return $array;
}

/**
 * Get app modules
 *
 * @param false $includesDashboard
 * @return array
 */
function getAllModules($includesDashboard = false)
{
    $array = [];

    // get all modules folder
    $modules = File::directories(app_path('Modules'));

    if (count($modules)) {
        foreach ($modules as $module) {

            // load modules service provider
            $config = require $module.'/config.php';

            if ($config['show_in_dashboard'] OR $includesDashboard) {
                $array[] = [
                    'name' => $config['name'],
                    'description' => $config['description'], // Just pust it here, maybe we need it in the future, yeah, maybe ...
                    'icon' => $config['icon'], // Just pust it here, maybe we need it in the future, yeah, maybe ...
                    'url' => $config['url'],
                    'permission' => isset($config['permission']) ? $config['permission'] : null,
                ];
            }
        }
    }

    return $array;
}
