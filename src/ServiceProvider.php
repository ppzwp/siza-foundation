<?php

namespace Siza\Foundation;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Laravel\Fortify\Contracts\LogoutResponse;
use Siza\Foundation\App\Http\Responses\LogoutResponse as SizaLogoutResponse;
//use Siza\Foundation\Console\ImportStaffImage;
use Siza\Foundation\Console\BackupCommand;
use Siza\Foundation\Console\InstallCommand;
use Siza\Foundation\Console\InstallModuleFiles;
use Siza\Foundation\Console\MakeModule;
use Siza\Foundation\Console\RestoreCommand;

class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/siza-foundation.php', 'siza-foundation');
        $this->mergeConfigFrom(__DIR__ . '/../config/siza-db.php', 'siza-db');

        if (file_exists(config_path('siza-db.php'))) {
            $this->mergeConfigFrom(config_path('siza-db.php'), 'database.connections');
        } else {
            $this->mergeConfigFrom(__DIR__ . '/../config/siza-db.php', 'database.connections');
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'siza');
        $this->loadViewsFrom(__DIR__ . '/../resources/ui', 'ui');
        $this->loadViewsFrom(__DIR__ . '/../resources/docs', 'docs');

        if (config('siza-foundation.load_routes')) {
            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        }

        $this->publishAsset();
        $this->publishConfig();
        $this->publishView();

        $this->consoleCommand();
        $this->bladeDirectives();

        $this->app->singleton(
            LogoutResponse::class,
            SizaLogoutResponse::class
        );

        require_once __DIR__.'/Helpers/functions.php';
    }

    private function publishView()
    {
        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/siza-foundation'),
        ], 'siza-foundation-view');

        $this->publishes([
            __DIR__ . '/../resources/ui' => resource_path('views/vendor/siza-ui'),
        ], 'siza-ui-view');
    }

    private function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/siza-db.php' => config_path('siza-db.php')
        ], 'siza-foundation-config');

        $this->publishes([
            __DIR__ . '/../config/siza-foundation.php' => config_path('siza-foundation.php')
        ], 'siza-foundation-config');
    }

    private function publishAsset()
    {
        $this->publishes([
            __DIR__ . '/../public/vendor/siza-foundation/css' => public_path('vendor/siza-foundation/css'),
        ], 'siza-foundation-asset');

        $this->publishes([
            __DIR__ . '/../public/vendor/siza-foundation/js' => public_path('vendor/siza-foundation/js'),
        ], 'siza-foundation-asset');

        $this->publishes([
            __DIR__ . '/../public/vendor/siza-foundation/fonts' => public_path('vendor/siza-foundation/fonts'),
        ], 'siza-foundation-asset');

        $this->publishes([
            __DIR__ . '/../public/vendor/siza-foundation/media' => public_path('vendor/siza-foundation/media'),
        ], 'siza-foundation-asset');
    }

    private function consoleCommand()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                BackupCommand::class,
                RestoreCommand::class,
                InstallModuleFiles::class,
//                ImportStaffImage::class,
                MakeModule::class
            ]);
        }
    }

    private function bladeDirectives()
    {
        Blade::directive('permission', function ($expression) {
            if (strpos($expression, '|')) {
                $permissions = explode('|', $expression);

                $array = [];

                foreach ($permissions as $perm) {
                    $array[] = auth()->user()->hasPermission($perm);
                }

                $permissions = implode(' OR ', $array);

                return "<?php"." if (" . $permissions . ") : ?>";
            }
            else {
                return "<?php if (auth()->user()->hasPermission($expression)) : ?>";
            }
        });

        Blade::directive('endpermission', function () {
            return "<?php"." endif;"." ?>";
        });
    }
}
