<?php

use Illuminate\Support\Facades\Route;

use Siza\Foundation\App\Http\Controllers\DashboardController;
use Siza\Foundation\App\Http\Controllers\LoginController;
use Siza\Foundation\App\Http\Controllers\LogoutController;

if (config('siza-foundation.sub_app') AND config('siza-foundation.use_remote_auth')) {
    Route::get('/login', function () {
        return redirect(config('siza-foundation.url').'/dashboard');
    })->middleware('guest')->name('login');

    Route::post('/login', [LoginController::class, 'callback'])
        ->middleware('web');

    Route::post('/callback/login', [LoginController::class, 'callback'])
        ->middleware('web');
}

Route::get('/', function () {
    if (! session()->has('key')) {
        session()->put('key', session()->getId());
    }

    return view('welcome');
})->middleware(['web']);

Route::group(['prefix' => 'siza', 'middleware' => 'web'], function () {
    Route::get('docs', function () {
        return view('docs::index');
    });

    Route::get('docs/grid', function () {
        return view('docs::grid');
    });

    Route::get('docs/typo', function () {
        return view('docs::typo');
    });

    Route::get('docs/tables', function () {
        return view('docs::tables');
    });
});




