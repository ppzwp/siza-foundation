<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Siza\Database\App\Models\Spsm\Employee;
use Symfony\Component\Process\Process;

class InstallModuleFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:module:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare module files and folders';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        (new Filesystem)->deleteDirectory(app_path('Modules'));

        (new Filesystem)->makeDirectory(app_path('Modules'));
        (new Filesystem)->makeDirectory(app_path('Modules/Dashboard'));
        (new Filesystem)->makeDirectory(app_path('Modules/Dashboard/Http'));
        (new Filesystem)->makeDirectory(app_path('Modules/Dashboard/Http/Controllers'));
        (new Filesystem)->makeDirectory(app_path('Modules/Dashboard/Routes'));
        (new Filesystem)->makeDirectory(app_path('Modules/Dashboard/Views'));

        (new Filesystem)->copy(__DIR__.'/../../stubs/Providers/ModuleServiceProvider.php', app_path('Providers/ModuleServiceProvider.php'));
        $this->installServiceProviderAfter('RouteServiceProvider', 'ModuleServiceProvider');

        (new Filesystem)->copy(__DIR__.'/../../stubs/Modules/Dashboard/ServiceProvider.txt', app_path('Modules/Dashboard/ServiceProvider.php'));
        (new Filesystem)->copy(__DIR__.'/../../stubs/Modules/Dashboard/config.txt', app_path('Modules/Dashboard/config.php'));
        (new Filesystem)->copy(__DIR__.'/../../stubs/Modules/Dashboard/Routes/web.txt', app_path('Modules/Dashboard/Routes/web.php'));
        (new Filesystem)->copy(__DIR__.'/../../stubs/Modules/Dashboard/Views/index.txt', app_path('Modules/Dashboard/Views/index.blade.php'));
        (new Filesystem)->copy(__DIR__.'/../../stubs/Modules/Dashboard/Http/Controllers/DashboardController.txt', app_path('Modules/Dashboard/Http/Controllers/DashboardController.php'));

        $this->line("<comment>MODULE</comment>: ".'Dashboard module installed.');
    }

    /**
     * Install the service provider in the application configuration file.
     *
     * @param  string  $after
     * @param  string  $name
     * @return void
     */
    protected function installServiceProviderAfter($after, $name)
    {
        if (! Str::contains($appConfig = file_get_contents(config_path('app.php')), 'App\\Providers\\'.$name.'::class')) {
            file_put_contents(config_path('app.php'), str_replace(
                'App\\Providers\\'.$after.'::class,',
                'App\\Providers\\'.$after.'::class,'.PHP_EOL.'        App\\Providers\\'.$name.'::class,',
                $appConfig
            ));
        }
    }
}