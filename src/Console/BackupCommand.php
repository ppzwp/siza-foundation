<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class BackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup all files and folders before installing package.';

    /**
     * To be use in command
     *
     * @var string
     */
    private $packageFolder = 'siza-foundation';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->backupFiles();

        $this->line('');
        $this->line("<comment>BACKUP</comment>: ".'Original files and folders has been backed-up.');
    }

    private function backupFiles()
    {
        if ((new Filesystem)->exists(storage_path($this->packageFolder))) {
            (new Filesystem)->deleteDirectory(storage_path($this->packageFolder));
        }

        (new Filesystem)->makeDirectory(storage_path($this->packageFolder));
        (new Filesystem)->makeDirectory(storage_path($this->packageFolder.'/backups'));
        (new Filesystem)->makeDirectory(storage_path($this->packageFolder.'/backups/app'));

        (new Filesystem)->copyDirectory(base_path('app'), storage_path($this->packageFolder.'/backups/app'));
        (new Filesystem)->copyDirectory(base_path('config'), storage_path($this->packageFolder.'/backups/config'));
        (new Filesystem)->copyDirectory(base_path('public'), storage_path($this->packageFolder.'/backups/public'));
        (new Filesystem)->copyDirectory(base_path('resources'), storage_path($this->packageFolder.'/backups/resources'));
        (new Filesystem)->copyDirectory(base_path('routes'), storage_path($this->packageFolder.'/backups/routes'));
        (new Filesystem)->copy(base_path('.env.example'), storage_path($this->packageFolder.'/.env.example'));
        (new Filesystem)->copy(base_path('.env'), storage_path($this->packageFolder.'/.env'));

        $this->compress();

        (new Filesystem)->deleteDirectory(storage_path($this->packageFolder));
    }

    private function compress(): void
    {
        $filePath = storage_path('/backup-'.microtime().'.zip');
        $zip = new \ZipArchive();

        if ($zip->open($filePath, \ZipArchive::CREATE) !== true) {
            throw new \RuntimeException('Cannot open ' . $filePath);
        }

        $this->addContent($zip, storage_path($this->packageFolder.'/backups'));
        $zip->close();
    }

    /**
     * This takes symlinks into account.
     *
     * @param ZipArchive $zip
     * @param string     $path
     */
    private function addContent(\ZipArchive $zip, string $path)
    {
        /** @var SplFileInfo[] $files */
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $path,
                \FilesystemIterator::FOLLOW_SYMLINKS
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        while ($iterator->valid()) {
            if (!$iterator->isDot()) {
                $filePath = $iterator->getPathName();
                $relativePath = substr($filePath, strlen($path) + 1);

                if (!$iterator->isDir()) {
                    $zip->addFile($filePath, $relativePath);
                } else {
                    if ($relativePath !== false) {
                        $zip->addEmptyDir($relativePath);
                    }
                }
            }
            $iterator->next();
        }
    }
}