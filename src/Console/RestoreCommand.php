<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class RestoreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore original files and folders.';

    /**
     * To be use in command
     *
     * @var string
     */
    private $packageFolder = 'siza-foundation';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->restoreFiles();

        $this->line('');
        $this->comment('Original files and folders has been restored.');
    }

    public function restoreFiles()
    {
//        (new Filesystem)->deleteDirectory(base_path('app'));
        (new Filesystem)->deleteDirectory(base_path('public'));
        (new Filesystem)->deleteDirectory(base_path('resources'));
        (new Filesystem)->deleteDirectory(base_path('routes'));

        (new Filesystem)->copyDirectory(storage_path($this->packageFolder.'/backups/app'), base_path('app'));
        (new Filesystem)->copyDirectory(storage_path($this->packageFolder.'/backups/config'), base_path('config'));
        (new Filesystem)->copyDirectory(storage_path($this->packageFolder.'/backups/public'), base_path('public'));
        (new Filesystem)->copyDirectory(storage_path($this->packageFolder.'/backups/resources'), base_path('resources'));
        (new Filesystem)->copyDirectory(storage_path($this->packageFolder.'/backups/routes'), base_path('routes'));
        (new Filesystem)->copy(storage_path($this->packageFolder.'/.env.example'), base_path('.env.example'));
        (new Filesystem)->copy(storage_path($this->packageFolder.'/.env'), base_path('.env'));

        (new Filesystem)->deleteDirectory(storage_path($this->packageFolder));
    }
}