<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Siza\Database\App\Models\Spsm\Employee;
use Symfony\Component\Process\Process;

class ImportStaffImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:staff-image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import staff image from SIZA database';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $employees = Employee::select('emp_id', 'emp_name')->whereNull('resign')->get();

        foreach ($employees as $employee) {
            $this->line($employee->emp_id .': '. $employee->emp_name);
            $this->importImage($employee);
        }
    }

    private function importImage($employee)
    {
        if (! file_exists(storage_path('app/public/avatar/'.$employee->emp_id.'.jpg'))) {
            if ($employee->image AND ! is_null($employee->image->gambar)) {
//                Storage::put( 'public/avatar/' . $employee->emp_id . '.bmp', $employee->image->gambar);
                File::put(storage_path('app/public/avatar/'.$employee->emp_id.'.jpg'), $employee->image->gambar);
                $this->info('Imported '.$employee->emp_id . '.jpg');
            }
            else {
                $this->error('Skipped '.$employee->emp_id . '.jpg');
            }
        }
    }
}