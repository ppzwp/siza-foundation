<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:install {--operation=install}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the SIZA foundation components and resources';

    /**
     * To be use in command
     *
     * @var string
     */
    private $packageFolder = 'siza-foundation';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->ask('Enter application name');

        $this->call('siza:backup');
        $this->vendorPublish();
        $this->installServiceProviders();
        $this->installModels();
        $this->installRoutes();
        $this->editMiddlewareFiles();
        $this->editEnvFiles($name);
        $this->editConfigFiles();
        $this->copyStubs();
        $this->installDashboardModule();

        $this->line('');
        $this->info('Installation process completed.');
    }

    /**
     * Replace a given string within a given file.
     *
     * @param  string  $search
     * @param  string  $replace
     * @param  string  $path
     * @return void
     */
    protected function replaceInFile($search, $replace, $path)
    {
        file_put_contents($path, str_replace($search, $replace, file_get_contents($path)));
    }

    /**
     * Install the service provider in the application configuration file.
     *
     * @param  string  $after
     * @param  string  $name
     * @return void
     */
    protected function installServiceProviderAfter($after, $name)
    {
        if (! Str::contains($appConfig = file_get_contents(config_path('app.php')), 'App\\Providers\\'.$name.'::class')) {
            file_put_contents(config_path('app.php'), str_replace(
                'App\\Providers\\'.$after.'::class,',
                'App\\Providers\\'.$after.'::class,'.PHP_EOL.'        App\\Providers\\'.$name.'::class,',
                $appConfig
            ));
        }
    }

    /**
     * Install the service provider in the application configuration file.
     *
     * @param  string  $name
     * @return void
     */
    protected function uninstallServiceProviderAfter($name)
    {
        if (Str::contains($appConfig = file_get_contents(config_path('app.php')), 'App\\Providers\\'.$name.'::class')) {
            file_put_contents(config_path('app.php'), str_replace(
                'App\\Providers\\'.$name.'::class,',
                '',
                $appConfig
            ));
        }
    }

    private function vendorPublish(): void
    {
        $this->callSilent('vendor:publish', ['--tag' => 'jetstream-config', '--force' => true]);
        $this->callSilent('vendor:publish', ['--tag' => 'fortify-config', '--force' => true]);
        $this->callSilent('sweetalert:publish', ['--force' => true]);

        $this->line("<comment>VENDOR</comment>: ".'Publishing vendor files has been completed.');
    }

    private function installServiceProviders(): void
    {
        // Service Providers...
        copy(base_path('/vendor/laravel/fortify/stubs/FortifyServiceProvider.php'), app_path('Providers/FortifyServiceProvider.php'));
        $this->installServiceProviderAfter('RouteServiceProvider', 'FortifyServiceProvider');

        copy(base_path('/vendor/laravel/jetstream/stubs/app/Providers/JetstreamServiceProvider.php'), app_path('Providers/JetstreamServiceProvider.php'));
        $this->installServiceProviderAfter('FortifyServiceProvider', 'JetstreamServiceProvider');

        $this->replaceInFile("'/home'", "'/dashboard'", app_path('Providers/RouteServiceProvider.php'));
        $this->replaceInFile("use Laravel\Fortify\Fortify;", "use Laravel\Fortify\Fortify;\nuse Siza\Foundation\App\Http\Responses\LogoutResponse as SizaLogoutResponse;", app_path('Providers/FortifyServiceProvider.php'));
        $this->replaceInFile("use Laravel\Fortify\Fortify;", "use Laravel\Fortify\Fortify;\nuse Laravel\Fortify\Http\Responses\LogoutResponse;", app_path('Providers/FortifyServiceProvider.php'));

        $bindLogoutResponse = file_get_contents(__DIR__.'/../../stubs/bind.txt');
        $this->replaceInFile("Fortify::resetUserPasswordsUsing(ResetUserPassword::class);", "\n".$bindLogoutResponse, app_path('Providers/FortifyServiceProvider.php'));

        $this->line("<comment>SERVICE PROVIDER</comment>: ".'Installing and modifiying service provider files has been completed.');
    }

    private function installModels(): void
    {
        (new Filesystem)->delete(app_path('Models/User.php'));
        copy(__DIR__ . '/../../stubs/Models/User.php', app_path('Models/User.php'));

        $this->line("<comment>MODELS</comment>: ".'Installing and modifiying User model has been completed.');
    }

    private function installRoutes(): void
    {
        (new Filesystem)->delete(base_path('routes/web.php'));
        copy(__DIR__ . '/../../stubs/routes/web.php', base_path('routes/web.php'));

        $this->line("<comment>ROUTES</comment>: ".'Installing new route file has been completed.');
    }

    private function editMiddlewareFiles(): void
    {
        $this->replaceInFile("//", "'/callback/login',", app_path('Http/Middleware/VerifyCsrfToken.php'));
        $this->replaceInFile("'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,", "'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,\n'permission' => \Siza\Foundation\App\Http\Middleware\ModulePermission::class,", app_path('Http/Kernel.php'));

        $this->line("<comment>MIDDLEWARE</comment>: ".'Modifiying middleware files has been completed.');
    }

    private function editEnvFiles($name): void
    {
        $files = [
            '.env', '.env.example'
        ];

        foreach ($files as $file) {
            $this->replaceInFile("APP_NAME=Laravel", "APP_NAME=\"" . $name . "\"", base_path($file));
            $this->replaceInFile("LOG_CHANNEL=stack", "SIZA_APP_URL=https://app.siza.my\nSIZA_APP_SUB=true\n\n" . "LOG_CHANNEL=stack", base_path($file));
            $this->replaceInFile("LOG_CHANNEL=stack", "LOG_CHANNEL=daily", base_path($file));
            $this->replaceInFile("DB_CONNECTION=mysql", "DB_CONNECTION=siza", base_path($file));
            $this->replaceInFile("DB_PORT=3306", "DB_PORT=1521", base_path($file));
            $this->replaceInFile("DB_USERNAME=root", "DB_USERNAME=ppz", base_path($file));
            $this->replaceInFile("DB_PASSWORD=", "DB_PASSWORD=ppzdev", base_path($file));
            $this->replaceInFile("DB_PASSWORD=ppzdev", "DB_PASSWORD=ppzdev\nDB_SSO_PASSWORD=", base_path($file));
            $this->replaceInFile("DB_PASSWORD=ppzdev", "DB_PASSWORD=ppzdev\nDB_SSO_USERNAME=root", base_path($file));
            $this->replaceInFile("DB_PASSWORD=ppzdev", "DB_PASSWORD=ppzdev\nDB_SSO_PORT=3306", base_path($file));
            $this->replaceInFile("DB_PASSWORD=ppzdev", "DB_PASSWORD=ppzdev\n\nDB_SSO_DATABASE=sso", base_path($file));
            $this->replaceInFile("DB_PASSWORD=ppzdev", "DB_PASSWORD=ppzdev\nDB_SSO_HOST=127.0.0.1", base_path($file));
        }

        $this->line("<comment>ENV FILES</comment>: ".'Modifiying .env and .env.example files has been completed.');
    }

    private function editConfigFiles(): void
    {
        $this->replaceInFile("'timezone' => 'UTC',", "'timezone' => 'Asia/Kuala_Lumpur',", config_path('app.php'));
        $this->replaceInFile("inertia", "livewire", config_path('jetstream.php'));
        $this->replaceInFile("Features::accountDeletion()", "// Features::accountDeletion()", config_path('jetstream.php'));
        $this->replaceInFile("'SESSION_DRIVER', 'database'", "'SESSION_DRIVER', 'file'", config_path('session.php'));

        $this->line("<comment>CONFIG FILES</comment>: ".'Modifiying config files has been completed.');
    }

    private function copyStubs(): void
    {
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/View', app_path('View'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../public/vendor/siza-foundation/css', public_path('css'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../public/vendor/siza-foundation/js', public_path('js'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../public/vendor/siza-foundation/fonts', public_path('fonts'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/resources/views/favicon', resource_path('views/favicon'));
        (new Filesystem)->copyDirectory(__DIR__ . '/../../stubs/resources/views/vendor', resource_path('views/vendor'));

        (new Filesystem)->copy(__DIR__ . '/../../public/vendor/siza-foundation/mix-manifest.json', public_path('mix-manifest.json'));
        (new Filesystem)->copy(__DIR__ . '/../../stubs/resources/views/navigation-menu.blade.php', resource_path('views/navigation-menu.blade.php'));
        (new Filesystem)->copy(__DIR__ . '/../../stubs/resources/views/dashboard.blade.php', resource_path('views/dashboard.blade.php'));
        (new Filesystem)->copy(__DIR__ . '/../../stubs/resources/views/sidebar.blade.php', resource_path('views/sidebar.blade.php'));
        (new Filesystem)->copy(__DIR__ . '/../../stubs/Controllers/DashboardController.php', app_path('Http/Controllers/DashboardController.php'));
        (new Filesystem)->copy(__DIR__ . '/../../stubs/Providers/AppServiceProvider.php', app_path('Providers/AppServiceProvider.php'));

        $this->line("<comment>PACKAGE</comment>: ".'Copying package files has been completed.');
    }

    private function installDashboardModule(): void
    {
        $this->call('siza:module:install');
    }
}