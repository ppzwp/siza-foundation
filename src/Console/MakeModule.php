<?php

namespace Siza\Foundation\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class MakeModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siza:make:module {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create module folder';

    /**
     * @var
     */
    private $modelName;

    /**
     * @var
     */
    private $nameField;

    /**
     * @var
     */
    private $names;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->ask('Enter module name');
        $force = $this->option('force');

        $modelName = $this->ask('Enter full model with namespace', 'App\Models\User');
        $nameField = $this->ask('Column name for display record', 'nama');

        // Forced add backslash at the beginning of model
        $modelName = "\\".ltrim($modelName, '\\\\');

        $names = $this->generateName($name, $modelName, $nameField);

        // check if folder exists
        if (File::exists(app_path('Modules/'.$names['folder_name'])) AND ! $force)  {
            $this->error('Module already exists!');
            return;
        }

        if (File::exists(app_path('Modules/'.$names['folder_name'])) AND $force)  {
            if ($this->confirm('This will delete existing folder. Proceed?')) {
                File::deleteDirectory(app_path('Modules/'.$names['folder_name']));
            }
        }

        // For acl prefix
        $parentId = $this->ask('Enter parent module ID');
        $aclPrefix = $this->ask('Enter prefix for permission name');

        $this->createFolders($names);
        $this->createServiceProvider($names);
        $this->createConfig($names);
        $this->createController($names);
        $this->createRoute($names, $aclPrefix);
        $this->createPermissions($names, $aclPrefix, $parentId);
        $this->createViews($names);
        $this->createExports($names);

        $this->comment($names['module_name'] . ' module created!');
    }

    /**
     * Create module folders
     */
    private function createFolders($names)
    {
        mkdir(app_path('Modules/'.$names['folder_name']));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Exports'));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Http'));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Http/Controllers'));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Routes'));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Views'));
        mkdir(app_path('Modules/'.$names['folder_name'].'/Views/exports'));
    }

    /**
     * Create module service provider
     */
    private function createServiceProvider($names)
    {
        // Load serviceprovider stub file
        $content = File::get(__DIR__.'/../../stubs/Modules/Base/ServiceProvider.txt');

        $content = $this->replaceStrings($names, $content);

        File::put(app_path('Modules/'.$names['folder_name'].'/ServiceProvider.php'), $content);
    }

    /**
     * Create module config file
     */
    private function createConfig($names)
    {
        // Load serviceprovider stub file
        $content = File::get(__DIR__.'/../../stubs/Modules/Base/config.txt');

        $content = $this->replaceStrings($names, $content);

        File::put(app_path('Modules/'.$names['folder_name'].'/config.php'), $content);
    }

    /**
     * Create module controllers
     */
    private function createController($names)
    {
        // Default controller
        $content = File::get(__DIR__.'/../../stubs/Modules/Base/Http/Controllers/controller.txt');
        $content = $this->replaceStrings($names, $content);
        File::put(app_path('Modules/'.$names['folder_name'].'/Http/Controllers/'.$names['folder_name'].'Controller.php'), $content);

        // Export Controller
        $content = File::get(__DIR__.'/../../stubs/Modules/Base/Http/Controllers/export.txt');
        $content = $this->replaceStrings($names, $content);
        File::put(app_path('Modules/'.$names['folder_name'].'/Http/Controllers/'.$names['folder_name'].'ExportController.php'), $content);
    }

    /**
     * Create module routes
     *
     * @param $aclPrefix
     */
    private function createRoute($names, $aclPrefix)
    {
        $content = File::get(__DIR__.'/../../stubs/Modules/Base/Routes/web.txt');

        $content = $this->replaceStrings($names, $content);
        $content = str_replace('[[PREFIX]]', $aclPrefix, $content);

        File::put(app_path('Modules/'.$names['folder_name'].'/Routes/web.php'), $content);
    }

    /**
     * Sync permissions into permissions table
     *
     * @param $names
     * @param $aclPrefix
     * @param $parentId
     */
    public function createPermissions($names, $aclPrefix, $parentId)
    {
        $namePrefix = strtoupper($aclPrefix);

        $permissions = [
            [
                'name' => $namePrefix.': '.$names['module_name'].' Index',
                'slug' => $aclPrefix.'.'.$names['view_namespace'].'.index'
            ],
            [
                'name' => $namePrefix.': '.$names['module_name'].' Create',
                'slug' => $aclPrefix.'.'.$names['view_namespace'].'.create'
            ],
            [
                'name' => $namePrefix.': '.$names['module_name'].' Edit',
                'slug' => $aclPrefix.'.'.$names['view_namespace'].'.edit'
            ],
            [
                'name' => $namePrefix.': '.$names['module_name'].' Delete',
                'slug' => $aclPrefix.'.'.$names['view_namespace'].'.delete'
            ],
        ];

        $count = rand(50, 300);

        foreach ($permissions as $permission) {
            $exists = DB::connection('sso')
                ->table('permissions')
                ->where('slug', $permission['slug'])
                ->exists();

            if (! $exists) {
                DB::connection('sso')
                    ->table('permissions')
                    ->insert([
                        'name' => $permission['name'],
                        'slug' => $permission['slug'],
                        'ordering' => $count++,
                        'module_id' => $parentId,
                        'active' => true,
                    ]);
            }
        }
    }

    /**
     * Create view files
     */
    private function createViews($names)
    {
        $files = [
            'index', 'create', 'edit', 'show',
        ];

        foreach ($files as $file) {
            // Load serviceprovider stub file
            $content = File::get(__DIR__ . '/../../stubs/Modules/Base/Views/'.$file.'.txt');

            $content = $this->replaceStrings($names, $content);

            File::put(app_path('Modules/' . $names['folder_name'] . '/Views/'.$file.'.blade.php'), $content);
        }
    }

    /**
     * Create export file
     */
    private function createExports($names)
    {
        $content = File::get(__DIR__ . '/../../stubs/Modules/Base/Exports/export.txt');
        $content = $this->replaceStrings($names, $content);
        File::put(app_path('Modules/' . $names['folder_name'] . '/Exports/'.$names['folder_name'].'Export.php'), $content);

        $content = File::get(__DIR__ . '/../../stubs/Modules/Base/Views/exports/senarai.txt');
        $content = $this->replaceStrings($names, $content);
        File::put(app_path('Modules/' . $names['folder_name'] . '/Views/exports/senarai.blade.php'), $content);
    }

    /**
     * Replace placeholders inside files with proper value
     *
     * @param $content
     * @return array|string|string[]
     */
    private function replaceStrings($names, $content)
    {
        $content = str_replace('[[FOLDER_NAME]]', $names['folder_name'], $content);
        $content = str_replace('[[MODULE_NAME]]', $names['module_name'], $content);
        $content = str_replace('[[VIEW_NAMESPACE]]', $names['view_namespace'], $content);
        $content = str_replace('[[MODEL_NAME]]', $names['model_name'], $content);
        $content = str_replace('[[MODEL_OBJECT]]', $names['model_object'], $content);
        $content = str_replace('[[NAME_FIELD]]', $names['name_field'], $content);
        $content = str_replace('[[NAME_FIELD_TITLE]]', $names['name_field_title'], $content);
        $content = str_replace('[[NAMESPACE]]', $names['namespace'], $content);
        $content = str_replace('[[DEFAULT_ROUTE]]', $names['view_namespace'].'.index', $content);

        return $content;
    }

    /**
     * Generate module element names
     *
     * @param $name
     * @param null $modelName
     * @param null $nameField
     * @return array
     */
    private function generateName($name, $modelName = null, $nameField = null)
    {
        // folder name must be TitleCased
        $moduleName = ucwords($name);

        // folder name must be TitleCased and without spaces
        $folderName = str_replace(' ', '', $moduleName);

        // Module namespace
        $namespace = "App\Modules\{$folderName}";

        // view namespace
        $viewNamespace = Str::slug($moduleName);

        $model_object = Str::camel($folderName);

        return [
            'module_name' => $moduleName,
            'model_name' => $modelName,
            'model_object' => $model_object,
            'folder_name' => $folderName,
            'name_field' => $nameField,
            'name_field_title' => ucwords($nameField),
            'namespace' => $namespace,
            'view_namespace' => $viewNamespace,
        ];
    }
}