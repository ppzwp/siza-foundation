<?php

return [
    // Main App URL
    'url' => env('SIZA_APP_URL', 'https://app.siza.my'),

    // Is this sub-application
    'sub_app' => env('SIZA_APP_SUB', true),

    // Enable query cache
    'enable_query_cache' => env('SIZA_ENABLE_QUERY_CACHE', true),

    // Query cache timeout
    'query_cache' => env('SIZA_QUERY_CACHE_TTL', 900),   // 15 minutes

    // Whether to load routes provides by package
    'load_routes' => env('SIZA_LOAD_ROUTE', true),

    // Use local or remote auth
    'use_remote_auth' => env('SIZA_REMOTE_AUTH', true),
];
