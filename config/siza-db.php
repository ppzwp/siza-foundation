<?php

return [
    'siza' => [
        'driver'         => 'oracle',
        'tns'            => env('DB_TNS', ''),
        'host'           => env('DB_HOST', '127.0.0.1'),
        'port'           => env('DB_PORT', '1521'),
        'database'       => env('DB_DATABASE', 'oracle'),
        'username'       => env('DB_USERNAME', 'user'),
        'password'       => env('DB_PASSWORD', 'password'),
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'edition'        => env('DB_EDITION', 'ora$base'),
        'server_version' => env('DB_SERVER_VERSION', '11g'),
    ],

    'sso' => [
        'driver' => 'mysql',
        'host' => env('DB_SSO_HOST', '127.0.0.1'),
        'port' => env('DB_SSO_PORT', '3306'),
        'database' => env('DB_SSO_DATABASE', 'forge'),
        'username' => env('DB_SSO_USERNAME', 'forge'),
        'password' => env('DB_SSO_PASSWORD', ''),
        'unix_socket' => env('DB_SSO_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => true,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],
];
